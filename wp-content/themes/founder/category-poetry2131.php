<div <?php post_class(); ?>>
	<?php do_action( 'archive_post_before' ); ?>
	<article>
		<a class='ui fluid card' href="<?php echo esc_url( get_permalink() ); ?>">
			<div class="image">
				<img src="<?php echo get_the_post_thumbnail_url(); ?>" style="max-height: 400px;">
			</div>
			<div class="content">
			    <h2 class="ui center header"><?php the_title(); ?></h2>
			    <div class="meta">
			      <span class="date"><?php
			      $date   = date_i18n( get_option( 'date_format' ), strtotime( get_the_date( 'c' ) ) );
$author = get_the_author();
		printf( esc_html__( 'Published %1$s by %2$s', 'founder' ), $date, $author ); ?></span>
			    </div>
			    <div class="description">
			      <?php echo get_the_excerpt(); ?>
			    </div>
			  </div>
			  <div class="extra content">
			  		<i class="comment icon"></i> <?php echo get_comments_number();?> comment(s)
			  </div>
		</a>
	</article>
	<?php do_action( 'archive_post_after' ); ?>
</div>
