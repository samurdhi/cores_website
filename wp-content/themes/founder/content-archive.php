<?php

if ( ! function_exists( ( 'get_first_paragraph' ) ) ) {
function get_first_paragraph(){
    global $post;
    $str = wpautop( get_the_content() );
    $str = substr( $str, 0, strpos( $str, '</p>' ) + 4 );
    //echo $str;
    //$str = strip_tags($str, '<a><strong><em>');
    return '<p>' . $str . '[...]</p>';
}

}

?>

<?php if(get_the_category()[0]->name == 'Poetry') : ?>
<div <?php post_class(); ?>>
	<?php do_action( 'archive_post_before' ); ?>
	<article class="poetry">
		<a class='ui fluid card' href="<?php echo esc_url( get_permalink() ); ?>">
			<div class="image">
				<img src="<?php echo get_the_post_thumbnail_url(); ?>" style="max-height: 400px;">
			</div>
			<div class="content">
			    <h2 class="ui center header"><?php the_title(); ?></h2>
			    <div class="meta">
			      <span class="date"><?php
			      $date   = date_i18n( get_option( 'date_format' ), strtotime( get_the_date( 'c' ) ) );
$author = get_the_author();
		printf( esc_html__( 'Published %1$s by %2$s', 'founder' ), $date, $author ); ?></span>
			    </div>
			    <div class="description">
			      <?php echo get_first_paragraph(); ?>
			    </div>
			  </div>
			  <div class="extra content">
			  		<i class="comment icon"></i> <?php echo get_comments_number();?> comment(s)
			  </div>
		</a>
	</article>
	<?php do_action( 'archive_post_after' ); ?>
</div>
<?php else: ?>
<div <?php post_class(); ?>>
	<?php do_action( 'archive_post_before' ); ?>
	<article>
		<a class='ui fluid card' href="<?php echo esc_url( get_permalink() ); ?>">
			<div class="image">
				<img src="<?php echo get_the_post_thumbnail_url(); ?>" style="max-height: 400px;">
			</div>
			<div class="content">
			    <h2 class="ui center header"><?php the_title(); ?></h2>
			    <div class="meta">
			      <span class="date"><?php
			      $date   = date_i18n( get_option( 'date_format' ), strtotime( get_the_date( 'c' ) ) );
$author = get_the_author();
		printf( esc_html__( 'Published %1$s by %2$s', 'founder' ), $date, $author ); ?></span>
			    </div>
			    <div class="description">
			      <?php echo get_the_excerpt(); ?>
			    </div>
			  </div>
			  <div class="extra content">
			  		<i class="comment icon"></i> <?php echo get_comments_number();?> comment(s)
			  </div>
		</a>
	</article>
	<?php do_action( 'archive_post_after' ); ?>
</div>
<?php endif;?>
