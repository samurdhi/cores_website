<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>Past SLPhOC Papers and Answers</h3>
        </div>

        <div class="canvas-body">
            <div class="row">
                <div class="table-wrapper">
                    <table class="ui celled unstackable table">
                        <thead>
                            <tr><th>Year</th>
                                <th>Paper(s)</th>
                                <th>Answers</th>
                            </tr></thead>
                        <tbody>
                            <?php
                            $string = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/data/past-papers-answers.json');
                            $past_papers = json_decode($string, true);


                            foreach ($past_papers as $past) {
                                echo '<tr>';
                                echo '<td>'.$past['year'].'</td>';
                                echo '<td>';
                                echo '<div class="compact button-array">';
                                if(!empty($past['papers']['english'])){
                                    echo '<a class="plain button" href="papers/'.$past['papers']['english'].'">English</a>';
                                }
                                if(!empty($past['papers']['sinhala'])){
                                    echo '<a class="plain button" href="papers/'.$past['papers']['sinhala'].'">Sinhala</a>';
                                }
                                if(!empty($past['papers']['tamil'])){
                                    echo '<a class="plain button" href="papers/'.$past['papers']['tamil'].'">Tamil</a>';
                                }
                                echo '</div>';
                                echo '</td>';
                                echo '<td>';
                                if(!empty($past['answers'])){
                                    echo '<a class="plain button" href="answers/'.$past['answers'].'">English</a>';
                                }
                                echo '</td>';
                                echo '</tr>';
                            }


                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>