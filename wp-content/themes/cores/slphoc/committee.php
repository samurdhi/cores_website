<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>Olympiad Committee</h3>
        </div>



        <div class="canvas-body">
            <div class="row">
                <p class="para">
                    The Institute of Physics Sri Lanka (IPSL) has appointed a committee for managing the activities related to Sri Lankan Physics Olympiad, National Astronomy and Astrophysics Olympiad, Sri Lankan Junior Olympaid as well as international olympiad events. This committee comprises of the following members.
                </p>
            </div>
            <div class="table-wrapper">
                <table class="ui celled unstackable table">
                    <tbody>


                        <?php
                        $col1 = ['Prof. S. R. D. Rosa',
                                 'Prof. K.P.S.C. Jayaratne',
                                 'Dr. R. V. Coorey',
                                 'Prof. K. Premaratne',
                                 'Prof. W. G. D. Dharmaratna',
                                 'Prof. S.R.D. Kalingamudali',
                                 'Dr. N. Pathmanathan',
                                 'Dr. N. Sivayogan'];
                        $col2 = ['National Coordinator - Physics Olympiad',
                                 'National Coordinator - Astronomy and Astrophysics Olympiad',
                                 'Examination Coordinator - University of Colombo',
                                 'Examination Coordinator - University of Peradeniya',
                                 'Examination Coordinator - University of Ruhuna',
                                 'Examination Coordinator - University of Kelaniya',
                                 'Examination Coordinator -  Eastern University',
                                 'Examination Coordinator -  University of Jaffna'];
                        $col3 = ['University of Colombo',
                                 'University of Colombo',
                                 'University of Colombo',
                                 'University of Peradeniya',
                                 'University of Ruhuna',
                                 'University of Kelaniya',
                                 'Eastern University of Sri Lanka',
                                 'University of Jaffna'];

                        foreach (range(0, count($col1)-1) as $index) {
                            echo '<tr>';
                            echo '<td>'.$col1[$index].'</td>';
                            echo '<td>'.$col2[$index].'</td>';
                            echo '<td>'.$col3[$index].'</td>';
                            echo '</tr>';
                        }


                        ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>