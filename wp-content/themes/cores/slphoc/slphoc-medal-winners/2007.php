<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>SLPhOC Medal Winners - 2007</h3>
        </div>

        <div class="canvas-body para">
            <div class="row">
                <div class="item-page">
                    <h3>Gold Medal Winners</h3>
                    <p><strong>Costha P, D..S.</strong>.Senanayake College, Colombo-7.</p>
                    <p><strong>Hevawitharana K. B.,</strong> Mahinda College, Galle.</p>
                    <p><strong>Karunarathne R. M. A.</strong>, Maliyadeva College, Kurunegala.</p>
                    <p><strong>Kotinkaduwa S. P. ,</strong> Wariyapola Sri Sumangala College, Kandy.</p>
                    <p><strong>Peiris P. N. P.,</strong> Ananda College, Colombo 10.</p>
                    <p><strong>Premaratne S. P.,</strong> Dharmaraja College, Kandy.</p>
                    <p><strong>Senthilnathan Sobisan,</strong> Jaffna Hindu College, Jaffna.</p>
                    <h3>Silver Medal Winners</h3>
                    <p><strong>De Silva A. T. A., S.</strong> Thomas' College, Mount Lavinia.</p>
                    <p><strong>De Silva E. C. L.,</strong> Ananda College, Colombo 10.</p>
                    <p><strong>Dhanushka L. G. G.,</strong> Dharmasoka College, Ambalangoda.</p>
                    <p><strong>Gayan J. M. U.,</strong> Maliyadeva College, Kurunegala.</p>
                    <p><strong>Jayaranga K. L. J.,</strong> Maliyadeva College, Kurunegala.</p>
                    <p><strong>Jayathilaka S. M. R. A.,</strong> Maliyadeva College, Kurunegala.</p>
                    <p><strong>Kandasamy K., S.</strong> Thomas' College, Mount Lavinia.</p>
                    <p><strong>Mendis T. C. Y.,</strong> Royal College, Colombo-7.</p>
                    <p><strong>Munchanayaka A. P.,</strong> Homagama CC, Homagama.</p>
                    <p><strong>Rajapakshe R. N. S.</strong>, Ananda College, Colombo 10.</p>
                    <p><strong>Ranathunga R. A. C.,</strong> Maliyadeva College, Kurunegala.</p>
                    <p><strong>Ranatunga R. P. R. L.,</strong> Sri Sumangala College, Panadura.</p>
                    <p><strong>Rubasinghe A. J.,</strong> Rahula College, Matara.</p>
                    <p><strong>Seneviratne S. A. A.,</strong> Rahula College, Matara.</p>
                    <p><strong>Thalaiyasingam Ajanthan,</strong> Jaffna Hindu College, Jaffna.</p>
                    <h3>Bronze Medal Winners</h3>
                    <p><strong>Shahathevan Vithoosan,</strong> Jaffna Hindu College, Jaffna.</p>
                    <p><strong>Vithanage K. P.,</strong> Royal College, Colombo-7.</p>
                    <p><strong>Dharmasiri D. R.,</strong> Vidyartha College, Kandy.</p>
                    <p><strong>Lalitharathne S. W. H. M. K. S.,</strong> Maliyadeva College, Kurunegala.</p>
                    <p><strong>Bandara D. M. A.,</strong> St. Anne's College, Kurunegalla.</p>
                    <p><strong>Amarasinghe A. M.,</strong> Kingswood College, Kandy.</p>
                    <p><strong>Miss. Wijesinghe P.,</strong> Mahamaya Girl's College, Kandy.</p>
                    <p><strong>Miss.Wickramarachchi W. A. T. K.,</strong> Visaka Vidyalaya, Colombo 5.</p>
                    <p><strong>Shanmugarajah Thavakumaran,</strong> Jaffna Central College, Jaffna.</p>
                    <p><strong>Balasubramanium Janarthanan,</strong> Jaffna Hindu College, Jaffna.</p>
                    <p><strong>Perera R., C. M.,</strong> Ananda College, Colombo - 10.</p>
                    <p><strong>Weerakoon C. S.,</strong> Royal College, Colombo-7.</p>
                    <p><strong>Bandara E. M. A.,</strong> Maliyadeva College, Kurunegala.</p>
                    <p><strong>Ranathunga S. Y. K.,</strong> Pinnawala NC, Rambukkana.</p>
                    <p><strong>Herath H. M. L. Y.,</strong> Vidyartha College, Kandy.</p>
                    <p><strong>Miss. Samarasinghe P. G. D. S. H.,</strong> Sujatha Vidyalaya, Matara.</p>
                    <p><strong>Miss.Amarasingha P. C.,</strong> Sujatha Vidyalaya, Matara.</p>
                    <p><strong>Ariyaratne B. P. P. V.,</strong> Rahula College, Matara.</p>
                    <p><strong>Ranasinghe D. I.,</strong> Nalanda College, Colombo-10.</p>
                    <p><strong>Jayaweera I. M. T. L.</strong>, Maliyadeva College, Kurunegala.</p>
                    <p><strong>Ramasinghe S. C.</strong>, Rahula College, Matara.</p>
                    <p><strong>Senaratne N. I.,</strong> Nalanda College, Colombo-10.</p>
                    <p><strong>Hettiarachchi B.,</strong> Royal College, Colombo-7.</p>
                    <p><strong>Mahathevan Brunthavan,</strong> Jaffna Hindu College, Jaffna.</p>

                </div>
            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>