<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>Medal Winners at SLPhOCs</h3>
        </div>

        <div class="canvas-body">
            <div class="row">
                <p class="para">
                    Every year, an island wide competition called “Sri Lankan Physics Olympiad Competition” is conducted by the Institute of Physics Sri Lanka for selecting young talented students to participate in the International Physics Olympiad (IPhO) and Asian Physics Olympiad (APhO) competitions.
                </p>
                <p class="para">
                    The national competition is held at the Universities of Colombo, Peradeniya, Kelaniya, Ruhuna, Jaffna and Batticaloa. The top performers at this competition are awarded medals and certificates by the Institute of Physics, Sri Lanka.
                </p>
                <p class="para">
                    The names of the award winners of the past 3 years can be found in below.
                </p>
            </div>
            <div class="row">
                    <div class="vertical-buttons delegations" style="padding: 0 20px;">
                        <?php
                        $medal_counts =['2005'=>[11,15,20], '2006'=>[5,19,54], '2007'=>[7,15,24]];
                        foreach ($medal_counts as $year=>$counts): ?>
                        <a class="plain button align-center align-middle" href="<?php echo $year.'.php';?>">
                            <label><?php echo $year;?></label>
                            <div class="ui mini statistics" style="display: inline-block; margin: 0; margin-left: 5px;">
                                <div class="statistic">
                                    <div class="value">
                                        <img src="<?php echo get_template_directory_uri();?>/assets/gold-medal.svg" class="ui circular inline image">
                                    </div>
                                    <div class="label">
                                        <?php echo $counts[0];?>
                                    </div>
                                </div>
                                <div class="statistic">
                                    <div class="value">
                                        <img src="<?php echo get_template_directory_uri();?>/assets/silver-medal.svg" class="ui circular inline image">
                                    </div>
                                    <div class="label">
                                        <?php echo $counts[1];?>
                                    </div>
                                </div>
                                <div class="statistic">
                                    <div class="value">
                                        <img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" class="ui circular inline image">
                                    </div>
                                    <div class="label">
                                        <?php echo $counts[2];?>
                                    </div>
                                </div>

                            </div>
                        </a>
                        <?php endforeach ?>
                    </div>
                </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>