<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>SLPhOC Medal Winners - 2005</h3>
        </div>

        <div class="canvas-body para">
            <div class="row">

                <div class="item-page">
                    <h3>Gold Medal Winners</h3>
                    <p><strong>Jayasumana H. M. K. G. S.,</strong> Kalutara Vidyalaya.</p>
                    <p><strong>Jayasinghe D. S.,</strong> Ananda College, Colombo-10.</p>
                    <p><strong>Chandrasekera C. M. R. B.</strong>, Maliyadeva College, Kurunegala.</p>
                    <p><strong>Ganganath M. A. N.,</strong> Maliyadeva College, Kurunegala.</p>
                    <p><strong>Bandara R. M. A. R.</strong>, St. Peter's College, Colombo-04.</p>
                    <p><strong>Gartheeban G.</strong>, Royal College, Colombo-07.</p>
                    <p><strong>Bulathsinghala S. V.,</strong> Asoka Vidyalaya.</p>
                    <p><strong>Amerasinghe A. A. B.</strong>, Hunumulla CC, Hunumulla.</p>
                    <p><strong>De Abrew K. P. S.</strong>, Nalanda College, Colombo-10.</p>
                    <p><strong>Wadduwage D. N.,</strong> St. Anne's College, Kurunegalla.</p>
                    <p><strong>Kudawithana D. U.</strong>, Maliyadeva College, Kurunegala.</p>
                    <p>&nbsp;</p>
                    <h3>Silver Medal Winners</h3>
                    <p><strong>Wickremasinghe W. M. A. S. B.</strong>, Maliyadeva College, Kurunegala.</p>
                    <p><strong>Ananthakumar A. S.</strong>, Thomas' College, Mt. Lavinia.</p>
                    <p><strong>Jayasiri V. P.</strong>, Royal College, Colombo-07.</p>
                    <p><strong>Gajasinghe R. W. R. L.</strong>, Bandarawella CC.</p>
                    <p><strong>Senaratne S. G. C.</strong>, Dharmasoka College, Ambalangoda.</p>
                    <p><strong>Gunawardhana W. D. T. L.,</strong>&nbsp;Dharmashoka College, Ambalangoda.</p>
                    <p><strong>Gallaba G. M. D. H</strong>., Nalanda College, Colombo-10.</p>
                    <p><strong>Silva G. A. N.,</strong>&nbsp;St. Joeseph's College, Colombo-10.</p>
                    <p><strong>Hettiarachchi S.</strong>, Royal College, Colombo-07.</p>
                    <p><strong>Thennakoon T. M. U. A. S.</strong>,Bandarawella CC.</p>
                    <p><strong>Illangakoon P. S.</strong>, Maliyadeva College, Kurunegala.</p>
                    <p><strong>Kusaladharma S. P.,</strong>&nbsp;Mahinda College, Galle.</p>
                    <p><strong>Rammiyatharshan S.,</strong>&nbsp;Sri Koneswara Hindu College, Trincomalee.</p>
                    <p><strong>Mayoorran S.,</strong>&nbsp;Jaffna Hindu College, Jaffna.</p>
                    <p><strong>Thaneshan S.,</strong>&nbsp;Jaffna Hindu College, Jaffna.</p>
                    <p>&nbsp;</p>
                    <h3>Bronze Medal Winners</h3>
                    <p><strong>Adikari A. A. A. S.</strong>, Devi Balika V, Colombo-08.</p>
                    <p><strong>Perera L. N. T.,</strong>&nbsp;Mahamaya Girls' College, Kandy.</p>
                    <p><strong>Nissanka S. N. A.,</strong>&nbsp;Royal College, Colombo-07.</p>
                    <p><strong>Samaratunga S. A. P. M.</strong>, Royal College, Colombo-07.</p>
                    <p><strong>Wasantha W. D. D.</strong>, Ananda College, Colombo-10.</p>
                    <p><strong>Mendis S. P.</strong>, Anula Vidyalaya, Nugegoda.</p>
                    <p><strong>Maldeniya B. S.,</strong>&nbsp;Ananda College, Colombo-10.</p>
                    <p><strong>Amerasinghe N. N.,</strong>&nbsp;Nalanda College, Colombo-10.</p>
                    <p><strong>Jayamanne M. D. C. J. P.,</strong>&nbsp;De Mazenod College, Kandana.</p>
                    <p><strong>Malasinghe M. A. U. S.,</strong>&nbsp;Devi Balika V, Colombo-08.</p>
                    <p><strong>Dasanayake N. M.</strong>, St. Benedict's College, Colombo-13.</p>
                    <p><strong>Priyalath H. P. N.</strong>, Nalanda College, Colombo-10.</p>
                    <p><strong>Kanchana W. G. P.</strong>, Ranabima Royal College, Peradeniya.</p>
                    <p><strong>Lansakara L. M. K. C.,</strong>&nbsp;Maliyadeva College, Kurunegala.</p>
                    <p><strong>Samarakoon S. T.</strong>, Maliyadeva College, Kurunegala.</p>
                    <p><strong>Chamara H. S. S.,</strong>&nbsp;St. Aloysius College, Galle.</p>
                    <p><strong>Galappatthi S. L. A.</strong>, Southlands College, Galle.</p>
                    <p><strong>Malinga A. W. S.</strong>, Dharmasoka College, Ambalangoda.</p>
                    <p><strong>Nadarajasarma S.</strong>, Jaffna Hindu College, Jaffna.</p>
                    <p><strong>Thavendra T.</strong>, BT/Periyakallar CC.</p>

                </div>

            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>