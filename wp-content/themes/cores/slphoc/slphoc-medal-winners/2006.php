<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>SLPhOC Medal Winners - 2006</h3>
        </div>

        <div class="canvas-body para">
            <div class="row">

                <div class="item-page">
                    <h3>Gold Medal Winners</h3>
                    <p><strong>Miss. S. Balasubramaniam,</strong> Chundikuli Girls College, Jaffna.</p>
                    <p><strong>T. R. Bandaragoda,</strong> Richmond College, Galle.</p>
                    <p><strong>A. U. Disanayake</strong>, Dharmasoka College, Ambalangoda.</p>
                    <p><strong>V. P. Jayasiri,</strong> Royal College, Colombo 7.</p>
                    <p><strong>G. J. Kottegoda,</strong> Ananda College, Colombo 10.</p>
                    <h3>Silver Medal Winners</h3>
                    <p><strong>A. Ananthkumar,</strong> S. Thomas’ College, Mount Lavinia.</p>
                    <p><strong>L. M. A. P. Cabral,</strong> Nalanda College, Colombo 10.</p>
                    <p><strong>Miss. U. D. C. Dilhari,</strong> Maliyadeva B.V., Kurunegalla.</p>
                    <p><strong>A. M. N. L. De Silva</strong>, Dharmasoka College, Ambalangoda.</p>
                    <p><strong>E. M. K. U. B. Ekanayaka,</strong> Maliyadeva College, Kurunegalla.</p>
                    <p><strong>B. H. M. M. T. Herath,</strong> Royal College, Colombo 7.</p>
                    <p><strong>H. A. C. H. Jayawardhana,</strong> Maliyadeva College, Kurunegalla.</p>
                    <p><strong>I. C. I. Kahandawaarachchi,</strong> Ananda College, Colombo 10.</p>
                    <p><strong>D. N. Kuruppu,</strong> Rahula Vidyalaya, Matara.</p>
                    <p><strong>R. B. H. Mahindaratne,</strong> Ananda College, Colombo 10.</p>
                    <p><strong>M. A. S. S. Mathotaarachchi,</strong> Ananda College, Colombo 10.</p>
                    <p><strong>M. A. S. P. Mudannayake,</strong> Ananda College, Colombo 10.</p>
                    <p><strong>S. Rammiyatharshan, R. K. M.</strong> Konoswara Hindu College, Trincomalee.</p>
                    <p><strong>S. Ratnayake,</strong> Rahula Vidyalaya, Matara.</p>
                    <p><strong>T. Suvarnarajah,</strong> American Mission College, Valvettithurai.</p>
                    <p><strong>A. Thevaher</strong>, St. Patrick's College, Patrick's Road, Jaffna.</p>
                    <p><strong>K. H. R. Thushara,</strong> Richmond College, Galle.</p>
                    <p><strong>W. M. B. S. Wijerathna,</strong> Maliyadeva College, Kurunegalla.</p>
                    <p><strong>J. A. R. Wimukthi,</strong> St. Anne's College.</p>
                    <h3>Bronze Medal Winners</h3>
                    <p><strong>M. N. S. Ariyasinghe,</strong> Ibbagamuwa Central College.</p>
                    <p><strong>H. M. S. T. Bandara,</strong> Maliyadeva College, Kurunegalla.</p>
                    <p><strong>Miss. W. M. M. Boteju,</strong> Visakha College, Colombo-5.</p>
                    <p><strong>M. G. K Chamara,</strong> Rahula Vidyalaya, Matara.</p>
                    <p><strong>C. H. Chandrasiri,</strong> Mahinda College, Galle.</p>
                    <p><strong>Miss. S. T. Cooray,</strong> Devi Balika Vidyalaya Colombo 8.</p>
                    <p><strong>J .C. A. Dahanayaka,</strong> Mahinda College, Galle.</p>
                    <p><strong>K. C. A. De Silva,</strong> Revata College, Balapitiya.</p>
                    <p><strong>Miss. Y. S. Dharmarathne,</strong> Musaeus College, Colombo-07.</p>
                    <p><strong>M. K. T. Dharmasena</strong>, Nalanda College, Colombo-10.</p>
                    <p><strong>H. A. Dissanayake,</strong> Nalanda College, Colombo-10.</p>
                    <p><strong>M. W. Edirisooriya,</strong> Mahanama College, Colombo 3.</p>
                    <p><strong>N. M. Ellawala,</strong> Ananda College, Colombo 10.</p>
                    <p><strong>M. R. Fawzer</strong>, St. Aloysius College, Galle.</p>
                    <p><strong>P. P. R. Fernando,</strong> Holy Cross College, Kalutara.</p>
                    <p><strong>S. Fernando,</strong> St. Benedicts College, Kotahena.</p>
                    <p><strong>K. B. Gallaba</strong>, Royal College, Colombo-7.</p>
                    <p><strong>C. N. K. Gamage,</strong> Rahula Vidyalaya, Matara.</p>
                    <p><strong>Miss. K. K. K. Gamage,</strong> Visakha College, Colombo-05.</p>
                    <p><strong>K. Ganeshananth,</strong> American Mission College, Valvettithurai.</p>
                    <p><strong>H. M. Gunasekara,</strong> D. S. Senanayaka College, Colombo 3.</p>
                    <p><strong>L. D. C. P. Gunasekera,</strong> Royal College, Colombo-7.</p>
                    <p><strong>S. S. Gunasekara,</strong> Royal College, Colombo-7.</p>
                    <p><strong>H. H. A. N. C. Hettiarachchi</strong>, Mayurapada C.C., Narammala</p>
                    <p><strong>Miss. A. A. Hewarathna,</strong> Devi Balika Vidyalaya, Colombo-8.</p>
                    <p><strong>R. J. M. K. A. Jayasundara,</strong> Mahanama College, Colombo-03.</p>
                    <p><strong>Miss. N. I. Jayawardana,</strong> Visakha College, Colombo-5.</p>
                    <p><strong>K. Kamalagoban,</strong> Hartley College, Point Pedro.</p>
                    <p><strong>Miss. L. Kandasamy,</strong> Vembadi Girls High School, Jaffna.</p>
                    <p><strong>L. G. T. Kaushalya,</strong> Rahula Vidyalaya, Matara.</p>
                    <p><strong>D. S. Kulathilake,</strong> De Mazenod College, Kandana.</p>
                    <p><strong>S. K. D. S. Kumara,</strong> Sri Dharmaloka Central College, Kelaniya.</p>
                    <p><strong>P. K. K. Madhawa,</strong> Mahinda College, Galle.</p>
                    <p><strong>J. E. M. C. B. Madugalle,</strong> Ananda College, Colombo 10.</p>
                    <p><strong>A. M. J. Maduranga,</strong> Senanayake Natonal School, Madampe.</p>
                    <p><strong>N. Mahinthakumar,</strong> Jaffna College, Vaddukkoddai.</p>
                    <p><strong>H. T. Nanayakkara,</strong> Ananda College, Colombo 10.</p>
                    <p><strong>Miss. K. P. K. Nanayakkara,</strong> Sirimavo Bandaranayake Vidyalaya, Colombo.</p>
                    <p><strong>M. D. P. Nirosh,</strong> Ananda College, Colombo 10.</p>
                    <p><strong>R. A. S. M. Perera,</strong> Ananda College, Colombo 10.</p>
                    <p><strong>M. D. D. B. Pothuhera,</strong> St. Anne's College.</p>
                    <p><strong>J. Ramanan,</strong> Hindu College, Chavakachcheri.</p>
                    <p><strong>D. I. Ranasinghe,</strong> Nalanda College, Colombo 10.</p>
                    <p><strong>T. D. H. Randeny</strong>, Royal College, Colombo-7.</p>
                    <p><strong>T. Serujanan,</strong> Jaffna Hindu College, Jaffna.</p>
                    <p><strong>K. Shribavan,</strong> Jaffna Hindu College, Jaffna.</p>
                    <p><strong>A. S. T. Somasiri,</strong> Royal College, Colombo-7.</p>
                    <p><strong>T. Suganthan,</strong> Hartley College, Point Pedro.</p>
                    <p><strong>Miss. R. Thirunavukkarasu</strong>, Vembadi Girls High School, Jaffna.</p>
                    <p><strong>Miss. D. T. A. Wahalathanthree,</strong> C. W. W. Kannangara M. M. V., Matugama.</p>
                    <p><strong>Miss. C. D. A. Weerasiriwardana,</strong> Maliyadeva BV, Kurunegalla.</p>
                    <p><strong>Miss. P. G. R. U. M. Welagedara,</strong> Visakha College, Colombo-5.</p>
                    <p><strong>Miss. P. D. C. I. Wijegunawardana,</strong> Visakha College, Colombo-5.</p>
                    <p><strong>R. D. Wijesinghe,</strong> Thurstan College, Colombo-3.</p>

                </div>

            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>