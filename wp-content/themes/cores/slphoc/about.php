<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>International Physics Olympiad Competitions</h3>
        </div>

        <div class="canvas-body">
            <div class="row">
                <p class="para">
                    Since 1967 the International Physics Olympiad (IPhO) competition has been held in different countries. The Asian Physics Olympiad (APhO) competition commenced in the year 2000. Participation in the Olympiads gives opportunities to young bright students from all over the world to share experiences and face challenges in solving Physics problems. It is considered an honour to any country if certificates and medals are won in these highly competitiveexaminations.

                    From the year 2005, the Institute of Physics in collaboration with the Department of Physics, University of Colombo and the Ministry of Education has made arrangements in preparing Sri Lankan A/L students for this competition.
                </p>

                <h4>
                    Nature of the competition
                </h4>

                <p class="para">
                    According to the Statutes of IPhO and APhO, each nation could send a team consisting of five students and eight students for the IPhO and APhO respectively. Students who have finished their university entrance examination can be members of the team only if they have not commenced their university studies at the time of the competition. The age of the contestants should not exceed 20 years on June 30th of the year the competition is held.

                    The Contestants have to sit for a five hour theory paper which generally includes three long questions. Apart from this, there will be a practical test which consists of one or two experiments. These questions are not easy and proper training is essential in order to achieve high marks. During the spare time the organizers will arrange sight-seeing tours, excursions and other cultural events so that the students are fully occupied.
                </p>

                <h4>Selection Procedure</h4>

                <div class="para">
                    <ol>
                        <li>Every year, an island - wide national competition called “Sri Lankan Physics Olympiad” is conducted by the IPSL. Students who are sitting for the Local/London A/L exams in that year are elligible to apply for this test. This test is open for students of both Physical and Biological Science streams, but a good knowledge in Mathematics is needed.</li>
                        <li>Every year, details and application forms are sent to all schools. As any number of students could apply from a particular school, teachers may photocopy the application forms for the use of the appropriate number of students. Alternatively, the application can be downloaded from here. Completed application forms should be mailed or hand delivered, together with the application fee, to the Department of Physics, University of Colombo.</li>
                        <li>The selection test held by the IPSL is a theory paper of two hour duration. The questions will be based on the local A/L physics syllabus. The questions are MCQ and semi structured type similar to the A/L questions, but will be little harder. The local competition will take place at the universities of Colombo, Peradeniya, Kelaniya, Ruhuna, Jaffna and Batticaloa. IPSL will inform the applicants personally the date and time of the exam. The top performers at this test will receive medals and certificates at an award ceremony held by the Institute of Physics, Sri Lanka.</li>
                        <li>Based on the marks obtained from this local competition, the best eight and five students will be selected to participate in the APho and IPhO respectively. They will be trained approximately for two months before the departure. The training camp will be held only at the University of Colombo. Therefore the selected students should be willing to come to Colombo daily during training. Unfortunately as the organizers cannot provide any accommodation, students will be required to arrange their own accommodation during the training period.</li>
                    </ol>
                </div>

                <h4>Medium of Instruction</h4>
                <p class="para">
                    The local Olympiad will be conducted in Sinhala, English and Tamil so that the students could select their preferred medium. The official language of the international competition is English. However if a student wants the paper in his/her mother tongue the leaders are entitled to translate the papers beforehand, so that there should not be any language barrier for the contestants to answer the papers.
                </p>

                <h4>Funds</h4>

                <p class="para">
                    The IPhO Organizing Committee normally covers the official expenses for the team, including meals and accommodation, during the official period. However, travel expenses between Sri Lanka and the country where IPho is held will not be their responsibility. In view of raising funds for this, we have opened a separate bank account at the Bank of Ceylon branch, Bambalapitiya.

                    If one wish to send money to the Physics Olympiad Fund the cheque should be drawn in favour of "Physics Olympiad Fund", and the cheque should be an "A/C Payee Only" cheque.

                    If anyone wishes to contribute to this cause he/she could send a cheque to us or send the contributions directly to the above bank account. It is worthwhile to note that the air fares of the students who participated in the IPhO in the past were sponsored mainly by either the parents or the teachers of the students and we value their contributions towards this noble course.

                    National Olympiad Committee
                    IPSL has appointed a committee to manage local activites related to IPhO and APhO. 
                </p>  

            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>