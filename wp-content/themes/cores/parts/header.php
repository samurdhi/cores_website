<!DOCTYPE html>
<html lang="en"><!-- <![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>CORES - UCLA</title>
    <meta name="description" content="Cognitive Reconfigurable Embedded Systems Lab is a unique research entity within UCLA, which focuses on research in Cognitive Radio and Cognitive Radio-like systems">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="theme-color" content="#1a2d4a">



    <link rel="icon" type="image/png" href="<?php echo get_template_directory();?>assets/cores_logo.png">
    <link rel="stylesheet" media="screen" href="<?php echo get_template_directory_uri();?>/eventscope_landing_files/web.min.css">
    <link rel="stylesheet" media="screen" href="<?php echo get_template_directory_uri();?>/eventscope_landing_files/form.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/eventscope_landing_files/style.css"> <!-- Resource style -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/eventscope_landing_files/segment.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/eventscope_landing_files/statistic.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/eventscope_landing_files/image.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/eventscope_landing_files/card.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/eventscope_landing_files/table.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/eventscope_landing_files/message.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/eventscope_landing_files/feed.min.css">
    <link href="<?php echo get_template_directory_uri();?>/eventscope_landing_files/bee3D.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/eventscope_landing_files/custom.css">
    <script src="<?php echo get_template_directory_uri();?>/eventscope_landing_files/analytics.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri();?>/eventscope_landing_files/clipboard.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri();?>/eventscope_landing_files/foundation.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;1,300;1,400&display=swap" rel="stylesheet">
    <!-- Hotjar Tracking Code for https://slpho.org -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1165524,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
    
    </head>

    <body id="home" class="" data-user-id="" data-user-email="" data-is-secure="true" data-domain="prismic.io">




        <!--
<aside class="top-banner-box">
<div class="row">
<div class="small-12 column">
<div class="top-banner-inner">

<div class="top-banner-tag">NEW</div>

<div class="top-banner-desc">
<p>prismic.io offers an ideal solution to feature your e-commerce products in your promotional landing pages or inspirational content. <a href="https://prismic.io/feature/integration-field">View more</a></p>
</div>
</div>
</div>
</div>
</aside>
-->




        <main>





            <section id="app-illustrations" class="hidden-preload">

                <!--
<div class="phone-big">
<img alt="digitalocean" src="<?php echo get_template_directory_uri();?>/img/v3/home/app-illustrations/digitalocean.svg">
</div>
<div class="phone-small">
<img alt="postmates" src="<?php echo get_template_directory_uri();?>/img/v3/home/app-illustrations/postmates.svg">
</div>
<div class="tablet-portrait">
<img alt="warbyparker" src="<?php echo get_template_directory_uri();?>/img/v3/home/app-illustrations/warbyparker.svg">
</div>
-->
            </section>








            <div class="header">



                <div class="header-container" style="padding-bottom: 100px;">

                    <div class="main-header">

                        <div class="row" style="padding-top: 20px;padding-bottom: 20px;">
                            <div class="align-middle align-center">
                                <img class="ui tiny image circular" src="<?php echo get_template_directory_uri();?>/assets/cores_logo.png"/ style="width: 390px; height: auto;">
                                <h2 style="margin-left: 20px; display: flex; align-items: center;">Cognitive Reconfigurable Embedded Systems Lab</h2>

                                <img class="ui tiny image" src="<?php echo get_template_directory_uri();?>/assets/ucla-logo.png"/ style="width: 100px; height: auto;">
                            </div>

                        </div>

                        <div class="row mobile-nav globalNav">
                            <ul class="navRoot globalPopupActive">
                                <li class="navSection logo" style="width: 90%;">
                                    <div style="padding-left: 20px; display: block; margin-top: 7.5px;">
                                        <h4 class="align-middle">
                                            <a href="<?php echo get_template_directory_uri();?>/"><img class="ui image circular" src="<?php echo get_template_directory_uri();?>/assets/cores_logo.png"/ style="height: 100%; margin-right: 10px; width: 80px; display: inline-block;"></a>
                                            <div style="display: inline-block;">
                                                CORES Lab - UCLA
                                            </div>

                                        </h4>
                                    </div>
                                </li>
                                <li class="navSection mobile">
                                    <a class="rootLink item-mobileMenu colorize"><h2>Menu</h2></a>
                                    <div class="popup">
                                        <div class="popupContainer">
                                            <a class="popupCloseButton">Close</a>
                                            <div class="mobileProducts" style="margin-top: 20px;">
                                                <h4>CORES Lab</h4>
                                                <div class="mobileProductsList">
                                                    <ul>
                                                        <li>
                                                            <a class="linkContainer item-payments" href="/" data-analytics-action="payments" data-analytics-source="mobile_nav">
                                                                Home
                                                            </a>
                                                        </li>
                                                        
                                                        <li>
                                                            <a class="linkContainer item-atlas" href="#" data-analytics-action="atlas" data-analytics-source="mobile_nav">
                                                                Courses
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <ul>
                                                       <li>
                                                            <a class="linkContainer item-payments" href="#" data-analytics-action="payments" data-analytics-source="mobile_nav">
                                                                People
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="linkContainer item-payments" href="<?php echo get_template_directory_uri();?>/news" data-analytics-action="payments" data-analytics-source="mobile_nav">
                                                                News
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="mobileProducts">
                                                <h4>Research</h4>
                                                <div class="mobileProductsList">
                                                    <ul>
                                                        <li>
                                                            <a class="linkContainer item-payments" href="#" data-analytics-action="payments" data-analytics-source="mobile_nav">
                                                                Research
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <ul>
                                                        <li>
                                                            <a class="linkContainer item-atlas" href="#" data-analytics-action="atlas" data-analytics-source="mobile_nav">
                                                                Publications
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="mobileSecondaryNav">
                                                <ul>
                                                    <li>
                                                        <a class="item-pricing" href="#" data-analytics-action="pricing" data-analytics-source="mobile_nav">
                                                            Events
                                                        </a>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>
                                                        <a class="item-jobs" href="#main-footer" data-analytics-source="mobile_nav" data-analytics-action="jobs">
                                                            Contact
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="navbar row" style="margin-top: 15px;">
                            <div class="align-middle">
                                <ul class="menu-desktop menu align-right align-middle row">
                                    <li class="menu__option" data-sub="home-tab"><a href="/fsd" id="home-tab">Hdfome</a></li>
                                    <li class="menu__item menu__option" data-sub="product"><a>News</a></li>
                                    <li class="menu__item menu__option" data-sub="resources"><a>People</a></li>
                                    <li class="menu__item menu__option" data-sub="about"><a>Publications</a></li>
                                    <li class="menu__option" data-sub="results-tab"><a href="#">Research</a></li>
                                    <li class="menu__option" data-sub="news-tab"><a href="#">Courses</a></li>
                                    <li class="menu__option" data-sub="news-tab"><a href="#">Events</a></li>
                                    <li class="menu__option" data-sub="contact-tab"><a href="#main-footer">Contact</a></li>
                                </ul>

                                <script>
                                    var data_sub = window.localStorage.getItem("ref");
                                    var currentActive = document.querySelector("li.menu__option .active");
                                    var flag=true;
                                    var others = [];
                                    document.querySelectorAll("li.menu__option").forEach(function(el){
                                        if(el.getAttribute("data-sub") === data_sub){
                                            el.firstChild.classList.remove("active");
                                            el.firstChild.classList.add("active");
                                            flag=false;
                                        }
                                        else{
                                            others.push(el);
                                        }
                                    });
                                    if(!flag){
                                        others.forEach(function(el){
                                            el.firstChild.classList.remove("active");
                                        });
                                    }
                                </script>

                                <div class="dropdown-holder">
                                    <div class="dropdown__arrow"></div>
                                    <div class="dropdown__bg">
                                        <div class="dropdown__bg-bottom"></div>
                                    </div>
                                    <div class="dropdown__wrap">
                                        <div class="dropdown-menu" id="product" data-sub="product">
                                            <div class="dropdown-menu__content">
                                                <div class="top-section">
                                                    <div class="col-1">
                                                        <ul>
                                                            <li><a href="<?php echo get_template_directory_uri();?>/news/">Current News</a></li>
                                                        </ul>  
                                                    </div>
                                                </div>
                                                <div class="bottom-section">
                                                        <h3>Archives</h3>
                                                        <ul>
                                                            <li><a href="#">2016/2018 UCLA Cores Lab News</a></li>
                                                            <li><a href="#">2013/2015 UCLA Cores Lab News</a></li>
                                                            <li><a href="#">2011/2012 UCLA Cores Lab News</a></li>
                                                            <li><a href="#">2010 UCLA Cores Lab News</a></li>
                                                            <li><a href="#">UCLA Cores Lab Historical Group Photographs</a></li>
                                                        </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dropdown-menu" id="developer" data-sub="developer">
                                            <div class="dropdown-menu__content">
                                                <div class="top-section" style="padding: 0;">
                                                    <div class="col-2 align-center">
                                                        <a href="<?php echo get_template_directory_uri();?>/performances/ipho" class="section-tile">
                                                            <h2 class="menu-title">IPhO</h2>

                                                            <div class="ui mini statistics">
                                                                <div class="statistic">
                                                                    <div class="value">
                                                                        <img src="<?php echo get_template_directory_uri();?>/assets/silver-medal.svg" class="ui circular inline image">
                                                                    </div>
                                                                    <div class="label">
                                                                        2
                                                                    </div>
                                                                </div>
                                                                <div class="statistic">
                                                                    <div class="value">
                                                                        <img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" class="ui circular inline image">
                                                                    </div>
                                                                    <div class="label">
                                                                        15
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="ui mini statistics">
                                                                <div class="statistic">
                                                                    <div class="value">
                                                                        <img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" class="ui inline image">
                                                                    </div>
                                                                    <div class="label">
                                                                        11
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <a href="<?php echo get_template_directory_uri();?>/performances/apho" class="section-tile">
                                                            <h2 class="menu-title">APhO</h2>
                                                            <div class="ui mini statistics">
                                                                <div class="statistic">
                                                                    <div class="value">
                                                                        <img src="<?php echo get_template_directory_uri();?>/assets/silver-medal.svg" class="ui circular inline image">
                                                                    </div>
                                                                    <div class="label">
                                                                        1
                                                                    </div>
                                                                </div>
                                                                <div class="statistic">
                                                                    <div class="value">
                                                                        <img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" class="ui circular inline image">
                                                                    </div>
                                                                    <div class="label">
                                                                        1
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="ui mini statistics">
                                                                <div class="statistic">
                                                                    <div class="value">
                                                                        <img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" class="ui inline image">
                                                                    </div>
                                                                    <div class="label">
                                                                        7
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="bottom-section info">
                                                    <p>See details of Sri Lanka's past delegations</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dropdown-menu" data-sub="resources">
                                            <div class="dropdown-menu__content" style="min-width: 450px;">
                                                <div class="top-section">
                                                    <h4>Faculty</h4>
                                                    <ul>
                                                        <li><a class="ui content"><img class="ui avatar image" src="<?php echo get_template_directory_uri();?>/assets/avatar/danijela.jpg">Danijela Cabric</a></li>
                                                    </ul>
        
                                                
                                                        <h4 style="margin-top: 20px;">PhD Students</h4>
                                                        <div class="row">
                                                                <div class="column">
                                                                    <ul>
                                                                        <li><a class="ui content"><img class="ui avatar image" src="<?php echo get_template_directory_uri();?>/assets/avatar/han.jpg">Han Yan</a></li>
                                                                        <li><a class="ui content"><img class="ui avatar image" src="<?php echo get_template_directory_uri();?>/assets/avatar/veljko.jpg">Veljko Boljanovic</a></li>
                                                                        <li><a class="ui content"><img class="ui avatar image" src="<?php echo get_template_directory_uri();?>/assets/avatar/samer.jpg">Samer Hanna</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="column">
                                                                    <ul>
                                                                        <li><a class="ui content"><img class="ui avatar image" src="<?php echo get_template_directory_uri();?>/assets/avatar/enes.jpg">Enes Krijestorac</a></li>
                                                                        <li><a class="ui content"><img class="ui avatar image" src="<?php echo get_template_directory_uri();?>/assets/avatar/samurdhi.jpg">Samurdhi Karunaratne</a></li>
                                                                        <li><a class="ui content"><img class="ui avatar image" src="<?php echo get_template_directory_uri();?>/assets/avatar/ben.jpg">Benjamin Domae</a></li>
                                                                    </ul>
                                                                </div>
                                                        </div>

                                                        <h4 style="margin-top: 20px;">Visitor</h4>
                                                        <ul>
                                                            <li><a class="ui content"><img class="ui avatar image" src="<?php echo get_template_directory_uri();?>/assets/avatar/agon.jpg">Agon Memedi</a></li>
                                                        </ul>
                                                        
                                                    </div>
                                                    <div class="bottom-section info">
                                                        <a href="#">All People</a>
                                                    </div>
                                                
                                            </div>
                                        </div>
                                        <div class="dropdown-menu" data-sub="about">
                                            <div class="dropdown-menu__content">
                                                <div class="top-section">
                                                    <ul>
                                                        <li><a href="#">Journals and Magazines</a></li>
                                                        <li><a href="#">Conference Proceedings</a></li>
                                                        <li><a href="#">Talks and Tutorials</a></li>
                                                        <li><a href="#">Submissions</a></li>
                                                    </ul>
                                                </div>
                                                <div class="bottom-section info">
                                                    <a href="#">All Publications</a>
                                                </div>
                                                <div class="bottom-section" style="display: none;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
