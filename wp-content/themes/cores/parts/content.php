<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment canvas">

        <div class="canvas-title" style="text-align: center;">
            <h3><?php the_title();?></h3>
        </div>
       
        <div class="canvas-body">
            <div>

            	<?php the_content();?>
            </div>
        </div>

    </div>

</div>