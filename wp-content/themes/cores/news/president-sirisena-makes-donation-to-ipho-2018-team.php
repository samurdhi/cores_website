<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>President Sirisena makes donation to IPhO 2018 team</h3>
        </div>

        <div class="canvas-body">
            <div class="row">
                <p class="para">Students to participate in 19th Asia Physics Olympiad in Vietnam receive monetary supplement from President.
                </p>
                <p class="para">The Sri Lankan group of eight students who are leaving for Vietnam to participate in the 19th Asia Physics Olympiad Competition received rupees one million in cash and gifts from President Maithripala Sirisena at the President’s Official Residence, today (04).</p>
                   
                <div class="small-12 large-6 large-centered columns news-image">
                    <img class="ui fluid image" src="<?php echo get_template_directory_uri();?>/assets/president_donation.jpg">
                    <label>President Sirisena makes donation to IPhO 2018 team</label>
                </div>
                
                <p class="para">The group of eight students comprised of Sujatha Vidyalaya, Matara, Eheliyagoda Central College, Richmond College, Galle, Royal College, Colombo, St. Thomas' College, Mount Lavinia and Maliyadeva College, Kurunegala will participate the 19th Asian Physics Olympiad (APhO 2018) which will take place in Hanoi, from 5th – 13th May 2018.</p>
                <p class="para">The President also posed for a group photograph with the students.</p>
            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>