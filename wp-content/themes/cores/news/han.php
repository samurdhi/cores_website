<?php include_once get_template_directory().'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>Han Yan wins the prestigious Qualcomm Innovation Fellowship</h3>
        </div>

        <div class="canvas-body">
            <div class="row">
                <p class="para">Han Yan, Shi Bu, and Saptadeep Pal have won the very selective 2019 Qualcomm Innovation Fellowship. Only 13 proposals out of 115 were selected nationwide for this fellowship. All three are PhD candidates in the UCLA Electrical and Computer Engineering Department.

The Qualcomm Innovation Fellowship recognizes and rewards top PhD students across a range of technical research areas.  Students apply and submit a proposal on any innovative idea of their choice. Winning students earn a one-year $100,000 fellowship and mentorship from Qualcomm engineers that help with the proposed research. Those who win exemplify Qualcomm’s core values of innovation, execution, and teamwork.
                <div class="small-12 large-6 large-centered columns news-image">
                    <img class="ui fluid image" src="<?php echo get_template_directory_uri();?>/assets/Han_Yan_Shi_Bu_and_Saptadeep_Pal.jpg">
                    <label>Han Yan and Shi Bu</label>
                </div>
                <p class="para">Han Yan’s work is conducted at the UCLA Cognitive Reconfigurable Embedded Systems (CORES) lab under his advisor Prof. Danijela Cabric. Shi Bu’s work is conducted at the UCLA Signal Processing and Circuit Engineering (SPACE) group under his advisor Prof. Sudhakar Pamarti. Together their research proposal is titled “A Wideband Frequency-Channelized ADC Using Time-Varying Circuits and Adaptive Digital Control.” This research focuses on developing analog-to-digital converters (ADCs) with GHz bandwidth and high resolutions for direct radio-frequency (RF) data acquisition.

Han and Shi’s research would enable signal aggregation from multiple communication bands/standards using only a single RF link. It would also allow for radios to be “upward compatible” with new communication protocols, future-proofing against modifications in the future. If successful, this research would reduce the complexity and power consumption of radios in mobile terminals, impacting strongly wireless communication in the era beyond 5G.

To achieve this, Han and Shi propose a frequency-channelized ADC architecture with three key innovations. First, they will intentionally use time-varying circuits as sharp RF filters to channelize the spectrum, which will handle “peaky” spectrum in aggregated wireless communications while the time-varying nature while help reconfiguration of the RF filters. Second, they will program the sub-ADCs to accommodate the dynamic range in their respective channels to fully adapt to the spectrum environment at will with minimum power. Third, they will develop new background calibration algorithms that will use received signals to handle circuits with non-idealities after reconfiguration. Together these attributes make up an ADC that has the potential to significantly advance the field of high speed ADC designs.</p>
            </div>
        </div>

    </div>

</div>

<?php include_once get_template_directory().'/parts/footer.php'; ?>