<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>Sri Lanka wins 4 Honorable Mentions at IPhO 2018</h3>
        </div>

        <div class="canvas-body">
            <div class="row">
                <p class="para">The Sri Lankan team won 4 Honourable Mention certificates at IPhO 2018 held in Lisbon, Portugal.
                </p>
                <div class="small-12 large-6 large-centered columns news-image">
                    <img class="ui fluid image" src="<?php echo get_template_directory_uri();?>/assets/ipho-2018-team.png">
                    <label>President Sirisena makes donation to IPhO 2018 team</label>
                </div>
                <p class="para">Congratulations to the Honourable Mention winners Mudith Witharama, Mandara Premalal, Yasod Sandeepa and Dilini Palihakkara. The team was also represented by Nimalanjana Piyumal Dassanayake. The team was accompanied by Dr. Ramal Coorey of the University of Colombo.</p>
            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>