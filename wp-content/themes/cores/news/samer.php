<?php get_header(); ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>Samer Hanna and Prof. Danijela Cabric won the Best Paper Award at IEEE ICNC 2019</h3>
        </div>

        <div class="canvas-body">
            <div class="row">
                <p class="para">Samer Hanna (Ph.D. student at UCLA CORES lab https://cores.ee.ucla.edu) and Prof. DanijelaCabric won the Best Paper Award at IEEE International Conference on Computing, Networking and Communication (ICNC), held in Honolulu Feb. 18-21, 2019, the premier conference in the computer and communications fields.
                <div class="small-12 large-6 large-centered columns news-image">
                    <img class="ui fluid image" src="<?php echo get_template_directory_uri();?>/assets/ICNCbestpaperAward.jpg">
                    <label>Samer Hanna and Prof. Danijela Cabric</label>
                </div>
                <p class="para">Their paper titled “Deep Learning Based Transmitter Identification using Power Amplifier Nonlinearity” presents a promising physical layer security approach for authenticating thousands of IoT devices based on their radio frequency (RF) fingerprints.</p>
            </div>
        </div>

    </div>

</div>

<?php include_once get_template_directory().'/parts/footer.php'; ?>