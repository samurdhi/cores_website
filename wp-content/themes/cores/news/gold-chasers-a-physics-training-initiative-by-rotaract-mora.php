<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>Gold Chasers: A physics training initiative by Rotaract Mora</h3>
        </div>

        <div class="canvas-body">
            <div class="row">
                <p class="para">‘Gold chasers’ , is a series of training sessions conducted for the Sri Lankan Team for the Asian Physics Olympiad and the International Physics Olympiad Competition for the year 2018. A highly skilled team from University of Moratuwa consisting of outstanding Rotaractors  with specialized knowledge in physics and have previously participated in Physics Olympiad were put together by project chairpersons as the mentors of the program.</p>
                
                <div class="small-12 large-6 large-centered columns news-image">
                    <img class="ui fluid image" src="<?php echo get_template_directory_uri();?>/assets/gold-chasers-rotaract-mora.jpg">
                </div>
                
                <p class="para">The 1st session of the project gold chasers was successfully held on  23rd of March 2018, at the Department of Physics in University of Colombo. Amidst all the challenges the  project was held in the common area in the Department of Physics, with the support and guidance of Prof. S.R.D.Rosa. It was a joyful event where the team from Rotaract Mora was able to meet, recognize and motivate the young, talented team members coming from every part of the country. The exceptional team memebrs even consisted of he Island 1st in Biology in 2017 A/L examination and many more top rankers.</p>
                <p class="para">The training session started with a descriptive, yet motivational  introduction about the International Physics Olympiad Arena to the new team members. In the morning session, Tharindu Samarakoon, a bronze medalist from the last year conducted a session on Basic Electro Magnetism and Couple of Problems were solved. Most of the concepts taught on that session was quite novel to this year’s team members, as there is a considerable gap between their school physics curriculum and the International Physics Olympiad Curriculum. But as our kids were smart and so enthusiastic, they could catch up the new theories and Concepts.</p>
                <p class="para">In the evening Session, Randula Abeyweera, one of the project chairs and the HM winner from last year conducted a session on Problem Solving Strategy. This session was to basically give a kick start and warm up for the team for the rigorous problem-solving skills which are required to achieve great in the International Competition. The new team members had to go through a set of problems which was bit challenging yet manageable.</p>
                <p class="para">Then the new team was introduced to the Moodle platform which was built by Gihan Chanaka Jayathilaka (2015 APhO and IPhO Team Member) specially for the SLPhO Team training .They were signed up to the moodle and they were informed about the upcoming online assignment and the resource sharing which would happen through the moodle platform. After Successful two sessions, the 1st day of Project Gold Chasers ended at 4.15 p.m.</p>
                <p class="para">Phase 1 of the Gold Chasers’ Training sessions will be conducted on a weekly basis and will be continued till the Asian Physics Olympiad which is happening on the 1st and 2nd weeks of May. Followed by the second phase that targets our main milestone, The International Physics Olympiad.</p>
            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>