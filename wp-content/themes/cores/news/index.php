<?php include_once get_template_directory().'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>CORES News</h3>
        </div>

        <div class="canvas-body news-array">
           
           <div class="news-array">
               <div class="row">

                <div class="small-12 medium-12 large-6 columns">
                    <a class="ui fluid card news-item" href="<?php echo get_template_directory_uri();?>/news/han.php">
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri();?>/assets/Han_Yan_Shi_Bu_and_Saptadeep_Pal.jpg">
                        </div>
                        <div class="content">
                            <h4 class="header">Han Yan wins the prestigious Qualcomm Innovation Fellowship</h4>
                        </div>
                    </a>
                </div>


                <div class="small-12 medium-12 large-6 columns">   
                    <a class="ui fluid card news-item" href="<?php echo get_template_directory_uri();?>/news/samer.php">
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri();?>/assets/ICNCbestpaperAward.jpg">
                        </div>
                        <div class="content">
                            <h4 class="header">CORES wins Best Paper Award at IEEE ICNC 2019</h4>
                        </div>
                    </a>
                </div>

                

            </div>
            
           </div>
            
        </div>

    </div>

</div>

<?php include_once get_template_directory().'/parts/footer.php'; ?>