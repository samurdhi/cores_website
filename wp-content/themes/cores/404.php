<?php get_header(); ?>

<div class="row" style="margin-top: 50px;">

	<div class="ui fluid segment canvas">

		<div class="canvas-title">
			<h3>That's an error!</h3>
		</div>

		<div class="canvas-body news-array">
			<img class="ui fluid image" src="<?php echo get_template_directory_uri();?>/assets/404.png">
		</div>

	</div>

</div>

<?php get_footer(); ?>