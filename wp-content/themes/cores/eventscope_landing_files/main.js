var menuItems = [].slice.call(document.querySelectorAll('.menu__item')),
    menuSubs = [].slice.call(document.querySelectorAll('.dropdown-menu')),
    selectedMenu = undefined,
    subBg = document.querySelector('.dropdown__bg'),
    subBgBtm = document.querySelector('.dropdown__bg-bottom'),
    subArr = document.querySelector('.dropdown__arrow'),
    subCnt = document.querySelector('.dropdown__wrap'),
    header = document.querySelector('.main-header'),
    potential_links = document.querySelectorAll('.navbar a'),
    dropdownHolder = document.querySelector('.dropdown-holder'),
    closeDropdownTimeout,

    startCloseTimeout = function startCloseTimeout() {
        closeDropdownTimeout = setTimeout(function () {return closeDropdown();}, 50);
    },

    stopCloseTimeout = function stopCloseTimeout() {
        clearTimeout(closeDropdownTimeout);
    },

    openDropdown = function openDropdown(el) {

        //- get menu ID
        var menuId = el.getAttribute('data-sub');
        //- get related sub menu
        var menuSub = document.querySelector('.dropdown-menu[data-sub="' + menuId + '"]');
        //- get menu sub content
        var menuSubCnt = menuSub.querySelector('.dropdown-menu__content');
        //- get bottom section of current sub
        //var menuSubBtm = menuSubCnt.querySelector('.bottom-section').getBoundingClientRect();
        //- get height of top section
        var menuSubTop = menuSubCnt.querySelector('.top-section').getBoundingClientRect();
        //- get menu position
        var menuMeta = el.getBoundingClientRect();
        //- get sub menu position
        var subMeta = menuSubCnt.getBoundingClientRect();
        
        dropdownHolder.style.pointerEvents = "initial";
        
        //- set selected menu
        selectedMenu = menuId;


        //- Remove active Menu
        menuItems.forEach(function (el) {return el.classList.remove('active');});
        //- Set current menu to active
        el.classList.add('active');

        //- Remove active sub menu
        menuSubs.forEach(function (el) {return el.classList.remove('active');});
        //- Set current menu to active
        menuSub.classList.add('active');

        //- Set dropdown menu background style to match current submenu style
        subBg.style.opacity = 1;
        subBg.style.left = menuMeta.left - (subMeta.width / 2 - menuMeta.width / 2) + 'px';
        subBg.style.width = subMeta.width + 'px';
        subBg.style.height = subMeta.height + 'px';
        //- Set dropdown menu bottom section background position
        subBgBtm.style.top = menuSubTop.height + 'px';
        //console.log(menuSubBtm);

        //- Set Arrow position
        subArr.style.opacity = 1;
        subArr.style.left = menuMeta.left + menuMeta.width / 2 - 10 + 'px';

        //- Set sub menu style
        subCnt.style.opacity = 1;
        subCnt.style.left = menuMeta.left - (subMeta.width / 2 - menuMeta.width / 2) + 'px';
        subCnt.style.width = subMeta.width + 'px';
        subCnt.style.height = subMeta.height + 'px';

        //- Set current sub menu style
        menuSub.style.opacity = 1;

        header.classList.add('dropdown-active');

    },
    closeDropdown = function closeDropdown() {

        dropdownHolder.style.pointerEvents = "none";
        
        //- Remove active class from all menu items
        menuItems.forEach(function (el) {return el.classList.remove('active');});
        //- Remove active class from all sub menus
        menuSubs.forEach(function (el) {
            el.classList.remove('active');
            el.style.opacity = 0;
        });
        //- set sub menu background opacity
        subBg.style.opacity = 0;
        //- set arrow opacity
        subArr.style.opacity = 0;


        // unset selected menu
        selectedMenu = undefined;

        header.classList.remove('dropdown-active');
    };

//- Binding mouse event to each menu items
menuItems.forEach(function (el) {

    //- mouse enter event
    el.addEventListener('mouseenter', function () {
        stopCloseTimeout();
        openDropdown(this);
    }, false);

    //- mouse leave event
    el.addEventListener('mouseleave', function () {return startCloseTimeout();}, false);

});

//- Binding mouse event to each sub menus
menuSubs.forEach(function (el) {

    el.addEventListener('mouseenter', function () {return stopCloseTimeout();}, false);
    el.addEventListener('mouseleave', function () {return startCloseTimeout();}, false);

});


potential_links.forEach(function (el){
    el.addEventListener('click', function(e){
        var hash = this.href.split("#")[1] || false;
        if(!hash && this.href.length !== 0){
            if(el.closest("li.menu__option") !== null){
                window.localStorage.setItem("ref", el.closest("li").getAttribute("data-sub"));
            }
            else{
                window.localStorage.setItem("ref", el.closest(".dropdown-menu").getAttribute("data-sub"));
            }
        }
    });
});




function readConfig(t) {
    var e = document.getElementById(t);
    if (!e)
        return null;
    var n, i = (n = (e.textContent || e.innerHTML).replace(/^\s+|\s+$/gm, ""),
    String(n).replace(/&quot;/g, '"').replace(/&#39;/g, "'").replace(/&#x2F;/g, "/").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&amp;/g, "&"));
    return /^[\],:{}\s]*$/.test(i.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, "")) ? window.JSON && window.JSON.parse ? window.JSON.parse(i) : new Function("return " + i)() : void 0
}
!function() {
    var t, e, n = "cookie_banner_ack";
    function i(e) {
        var i, o;
        i = new Date,
        o = n + "=ack",
        i.setYear(i.getFullYear() + 10),
        o += ";expires=" + i.toGMTString(),
        o += ";domain=" + document.domain,
        document.cookie = o,
        t.classList.add("dismissed")
    }
    document.addEventListener("DOMContentLoaded", function() {
        t = document.querySelector('[rel="cookie-notification"]'),
        (e = document.querySelector('[rel="dismiss-cookie-notification"]')) && e.addEventListener("click", i)
    })
}(),
window.$ && window.$.ajaxPrefilter && $(function() {
    var t;
    return t = function() {
        var t, e;
        return (t = $("form input[name=csrf-token]")).length > 0 ? t.attr("value") : (e = $("meta[name=csrf-token]")).length > 0 ? e.attr("content") : ""
    }
    ,
    $.ajaxPrefilter(function(e, n, i) {
        var o;
        return o = t(),
        i.setRequestHeader("x-stripe-csrf-token", o)
    })
});
var Strut = {
    random: function(t, e) {
        return Math.random() * (e - t) + t
    },
    arrayRandom: function(t) {
        return t[Math.floor(Math.random() * t.length)]
    },
    interpolate: function(t, e, n) {
        return t * (1 - n) + e * n
    },
    rangePosition: function(t, e, n) {
        return (n - t) / (e - t)
    },
    clamp: function(t, e, n) {
        return Math.max(Math.min(t, n), e)
    },
    queryArray: function(t, e) {
        return e || (e = document.body),
        Array.prototype.slice.call(e.querySelectorAll(t))
    },
    ready: function(t) {
        "loading" !== document.readyState ? t() : document.addEventListener("DOMContentLoaded", t)
    },
    debounce: function(t, e) {
        let n;
        return function() {
            clearTimeout(n),
            n = setTimeout(function() {
                return t.apply(this, arguments)
            }, e)
        }
    },
    throttle: function(t, e, n) {
        var i = n || this
          , o = null
          , a = null
          , r = function() {
            t.apply(i, a),
            o = null
        };
        return function() {
            o || (a = arguments,
            o = setTimeout(r, e))
        }
    }
};

function globalNavPopup(t) {
    var e = this
      , n = Strut.touch.isSupported ? "touchend" : "click";
    this.activeClass = "globalPopupActive",
    this.root = document.querySelector(t),
    this.link = this.root.querySelector(".rootLink"),
    this.popup = this.root.querySelector(".popup"),
    this.closeButton = this.root.querySelector(".popupCloseButton"),
    this.link.addEventListener(n, function(t) {
        t.stopPropagation(),
        e.togglePopup()
    }),
    this.popup.addEventListener(n, function(t) {
        t.stopPropagation()
    }),
    //alert(n+" "+this.popup),
    this.popup.addEventListener("transitionend", function(t) {
        //alert("ffsfs");
        if (e.isOpening) {
            e.isOpening = !1;
            var n = e.popup.getBoundingClientRect().top + window.scrollY;
            if (n < 15) {
                var i = 15 - n;
                e.popup.style.transform = "translateY(" + i + "px)"
            }
        }
    }),
    this.closeButton && this.closeButton.addEventListener(n, function(t) {
        e.closeAllPopups()
    }),
    document.body.addEventListener(n, function(t) {
        Strut.touch.isDragging || e.closeAllPopups()
    }, !1)
}
Strut.isRetina = window.devicePixelRatio > 1.3,
Strut.mobileViewportWidth = 670,
Strut.isMobileViewport = window.innerWidth < Strut.mobileViewportWidth,
window.addEventListener("resize", function() {
    Strut.isMobileViewport = window.innerWidth < Strut.mobileViewportWidth
}),
Strut.touch = {
    isSupported: "ontouchstart"in window || navigator.maxTouchPoints,
    isDragging: !1
},
document.addEventListener("DOMContentLoaded", function() {
    document.body.addEventListener("touchmove", function() {
        Strut.touch.isDragging = !0
    }),
    document.body.addEventListener("touchstart", function() {
        Strut.touch.isDragging = !1
    })
}),
Strut.load = {
    images: function(t, e) {
        "string" == typeof t && (t = [t]);
        var n = -t.length;
        t.forEach(function(t) {
            var i = new Image;
            i.src = t,
            i.onload = function() {
                0 === ++n && e && e()
            }
        })
    },
    css: function(t, e) {
        var n = document.createElement("link")
          , i = (window.readConfig("strut_files") || {})[t];
        if (!i)
            throw new Error('CSS file "' + t + '" not found in strut_files config');
        n.href = i,
        n.rel = "stylesheet",
        document.head.appendChild(n),
        e && (n.onload = e)
    },
    js: function(t, e) {
        var n = document.createElement("script")
          , i = (window.readConfig("strut_files") || {})[t];
        if (!i)
            throw new Error('Javascript file "' + t + '" not found in strut_files config');
        n.src = i,
        n.async = !1,
        document.head.appendChild(n),
        e && (n.onload = e)
    }
},
Strut.supports = {
    es6: !!window.Symbol && !!window.Symbol.species,
    pointerEvents: function() {
        var t = document.createElement("a").style;
        return t.cssText = "pointer-events:auto",
        "auto" === t.pointerEvents
    }(),
    positionSticky: Boolean(window.CSS && CSS.supports("(position: -webkit-sticky) or (position: sticky)")),
    masks: !/MSIE|Trident|Edge/i.test(navigator.userAgent)
},
globalNavPopup.prototype.togglePopup = function() {
    var t = this.root.classList.contains(this.activeClass);
    this.closeAllPopups(!0),
    t || (this.root.classList.add(this.activeClass),
    this.isOpening = !0)
}
,
globalNavPopup.prototype.closeAllPopups = function(t) {
    for (var e = document.getElementsByClassName(this.activeClass), n = 0; n < e.length; n++)
        e[n].querySelector(".popup").style.transform = null,
        e[n].classList.remove(this.activeClass)
}
,
Strut.ready(function() {
    new globalNavPopup(".globalNav .navSection.mobile")
}),
function() {
    "use strict";
    window.siteAnalytics = window.siteAnalytics || {},
    window.siteAnalyticsUtil = window.siteAnalyticsUtil || {};
    var t, e = !1, n = "SITE_ANALYTICS_DEBUG", i = [], o = 250, a = o, r = 1.3;
    function s() {
        var t = [].slice.call(arguments);
        w() && console.log.apply(console, t)
    }
    function c() {
        return window.readConfig("site-analytics-config") || {}
    }
    function l() {
        return c().generalAnalyticsConfig || {}
    }
    function d() {
        return c().siteSpecificAnalyticsConfig || {}
    }
    function u() {
        return d().gtmConfig || {}
    }
    function w() {
        return !!window[n]
    }
    function p(t, e) {
        m("action", t, e)
    }
    function f(t, e) {
        m("actionOnce", t, e)
    }
    function g(t, e) {
        m("modal", t, e)
    }
    function y(t, e) {
        m("viewed", t, e)
    }
    function m(t, e, n) {
        window.Analytics ? h(t, e, n) : function(t, e, n) {
            i.push([t, e, n]),
            b(),
            s("enqueue", t, e, n)
        }(t, e, n)
    }
    function h(t, e, n) {
        A();
        var i = function(t) {
            var e = l();
            return Object.keys(t || {}).forEach(function(n) {
                e[n] = t[n]
            }),
            e
        }(n);
        window.Analytics[t](e, i),
        s("emit", t, e, i)
    }
    function v() {
        var t, e, n = u();
        t = n,
        e = {},
        Object.keys(t).forEach(function(n) {
            var i = t[n];
            e[n] = i || void 0
        }),
        n = e,
        window.dataLayer = window.dataLayer || [],
        window.dataLayer.push(n)
    }
    function A() {
        e || (window.Analytics.configure(l()),
        e = !0,
        s("Sent config data"))
    }
    function b() {
        t || (t = setTimeout(k, a),
        a *= r)
    }
    function k() {
        t = null,
        window.Analytics ? (s("Flushing event queue"),
        A(),
        i.forEach(function(t) {
            h.apply(this, t)
        }),
        a = o) : (b(),
        s("Ready timer waiting " + a + "ms"))
    }
    function C(t) {
        if (!window.ga)
            return window.siteAnalyticsUtil.enqueuedCalls = window.siteAnalyticsUtil.enqueuedCalls || [],
            void window.siteAnalyticsUtil.enqueuedCalls.push({
                fnName: "sendToGoogleAnalytics",
                args: [t]
            });
        window.ga.apply(this, t),
        s("ga", t)
    }
    function L(t) {
        window.dataLayer.push(t),
        s("dataLayer", t)
    }
    function S() {
        window.ga && window.siteAnalyticsUtil.generalAnalyticsConfig ? window.siteAnalyticsUtil.enqueuedCalls && (window.siteAnalyticsUtil.enqueuedCalls.forEach(function(t) {
            var e = window.siteAnalyticsUtil[t.fnName];
            e ? e.apply(window.siteAnalyticsUtil, t.args) : setTimeout(function() {
                throw Error("siteAnalyticsUtil has no fn " + t.fnName)
            })
        }),
        window.siteAnalyticsUtil.enqueuedCalls = []) : setTimeout(S, 100)
    }
    window.siteAnalyticsUtil.analyticsConfigData || (v(),
    b(),
    window.siteAnalyticsUtil.debugActive = w,
    window.siteAnalyticsUtil.emitAction = p,
    window.siteAnalyticsUtil.emitActionOnce = f,
    window.siteAnalyticsUtil.emitModal = g,
    window.siteAnalyticsUtil.emitViewed = y,
    window.siteAnalyticsUtil.siteAnalyticsConfig = d,
    window.siteAnalyticsUtil.siteGtmConfigData = u,
    window.siteAnalyticsUtil.sendToGoogleAnalytics = C,
    window.siteAnalyticsUtil.sendToDataLayer = L,
    window.siteAnalyticsUtil.generalAnalyticsConfig = l,
    S())
}(),
function() {
    var t = "data-analytics-action"
      , e = "data-analytics-source"
      , n = "data-analytics-modal"
      , i = "data-analytics-ga";
    function o(e) {
        return e && e.getAttribute ? e.getAttribute(t) || e.getAttribute(n) ? e : e.parentNode && "BODY" !== e.tagName ? o(e.parentNode) : null : (setTimeout(function() {
            window.Raven && window.Raven.captureMessage("Tried to findAnalyticsAttributes on something without .getAttribute (old code)", {
                extra: {
                    el: e
                }
            })
        }, 2e3),
        null)
    }
    function a(t) {
        return !!o(t)
    }
    function r(a) {
        var r = function(a) {
            var r = o(a)
              , s = {};
            return r.getAttribute(t) && (s.action = r.getAttribute(t)),
            r.getAttribute(n) && (s.modal = r.getAttribute(n)),
            r.getAttribute(e) && (s.params = {
                source: r.getAttribute(e)
            }),
            r.getAttribute(i) && (s.googleAnalyticsParams = JSON.parse(r.getAttribute(i))),
            r.getAttribute(e) && -1 !== r.getAttribute(e).indexOf("cta") && (s.trackCta = !0),
            s
        }(a);
        r.modal && window.siteAnalyticsUtil.emitModal(r.modal, r.params),
        r.action && window.siteAnalyticsUtil.emitAction(r.action, r.params),
        r.googleAnalyticsParams && window.siteAnalyticsUtil.sendToGoogleAnalytics(r.googleAnalyticsParams),
        r.trackCta && s(a)
    }
    function s(n) {
        var i = n.getAttribute(t) + "_" + n.getAttribute(e);
        window.siteAnalyticsUtil.sendToDataLayer({
            event: "cta-button-click",
            "cta-type": i,
            "click-url": n.href
        })
    }
    window.siteAnalytics.hasAnalyticsAttributes = a,
    window.siteAnalytics.trackByAttributes = r,
    window.siteAnalytics.trackCtaClick = s
}(),
function() {
    function t(t) {
        if (t.currentTarget.hasAttribute("data-language")) {
            var e = t.currentTarget.getAttribute("data-language");
            window.siteAnalytics.trackLanguageChange(e)
        }
    }
    function e(t) {
        if (t.currentTarget.hasAttribute("data-country")) {
            var e = t.currentTarget.getAttribute("data-country");
            window.siteAnalytics.trackCountryChange(e)
        }
    }
    document.addEventListener("DOMContentLoaded", function() {
        [].slice.call(document.querySelectorAll(".languagePicker a[data-language]")).forEach(function(e) {
            e.addEventListener("click", t)
        }),
        [].slice.call(document.querySelectorAll(".countryPicker a[data-country]")).forEach(function(t) {
            t.addEventListener("click", e)
        })
    })
}(),
function() {
    var t = "form_input"
      , e = "form_submit";
    function n(e) {
        e.target.matches("form *") && window.siteAnalyticsUtil.emitAction(t, {
            name: e.target.getAttribute("name"),
            value: e.target.value
        })
    }
    function i(t) {
        "FORM" === t.target.tagName && window.siteAnalyticsUtil.emitAction(e)
    }
    Element.prototype.matches || (Element.prototype.matches = Element.prototype.msMatchesSelector),
    document.addEventListener("change", n),
    document.addEventListener("submit", i)
}(),
function() {
    var t = "nav_dropdown_open"
      , e = {};
    function n(n) {
        e[n] || (e[n] = !0,
        window.siteAnalyticsUtil.emitAction(t, {
            dropdown: n
        }))
    }
    window.siteAnalytics.trackGlobalNavDropdownOpen = n
}(),
function(t, e, n, i, o) {
    t[i] = t[i] || [],
    t[i].push({
        "gtm.start": (new Date).getTime(),
        event: "gtm.js"
    });
    var a = e.getElementsByTagName(n)[0]
      , r = e.createElement(n);
    r.async = !0,
    r.src = "https://www.googletagmanager.com/gtm.js?id=GTM-K8JKCBR",
    a.parentNode.insertBefore(r, a)
}(window, document, "script", "dataLayer"),
function() {
    var t = "radar_icosahedron"
      , e = "radar_fraud_chart"
      , n = "home_page_notebook"
      , i = "connect_routing_diagram"
      , o = "billing_infra_diagram"
      , a = "billing_node_open"
      , r = "billing_interactive_invoice_section"
      , s = "query_category"
      , c = "pricing_slider"
      , l = {}
      , d = {}
      , u = {}
      , w = {}
      , p = {};
    function f() {
        window.siteAnalyticsUtil.emitActionOnce(t)
    }
    function g() {
        window.siteAnalyticsUtil.emitActionOnce(e)
    }
    function y(t) {
        var e = t.innerText.trim().toLowerCase();
        l[e] || (l[e] = !0,
        window.siteAnalyticsUtil.emitAction(n, {
            text: e
        }))
    }
    function m(t) {
        var e = t.innerText.trim().toLowerCase();
        d[e] || (l[e] = !0,
        window.siteAnalyticsUtil.emitAction(i, {
            text: e
        }))
    }
    function h(t) {
        var e = t.innerText.trim().toLowerCase();
        u[e] || (u[e] = !0,
        window.siteAnalyticsUtil.emitAction(o, {
            text: e
        }))
    }
    function v(t, e) {
        var n = t.dataset.nodeType
          , i = t.dataset.nodeDescription
          , o = t.querySelector(".infra-node__meta").textContent.trim().toLowerCase()
          , r = e + "-" + n + "-" + o;
        w[r] || (w[r] = !0,
        window.siteAnalyticsUtil.emitAction(a, {
            infra_selectedPlan: e,
            infra_node_type: n,
            infra_node_meta: o,
            infra_node_description: i
        }))
    }
    function A(t) {
        var e = t.dataset.target;
        p[e] || (p[e] = !0,
        window.siteAnalyticsUtil.emitAction(r, {
            section: e
        }))
    }
    function b(t) {
        window.siteAnalyticsUtil.emitAction(s, {
            category: t
        })
    }
    function k(t) {
        window.siteAnalyticsUtil.emitAction(s, {
            query: t
        })
    }
    function C() {
        window.siteAnalyticsUtil.emitActionOnce(c)
    }
    function L() {
        window.siteAnalyticsUtil.sendToDataLayer({
            event: "virtual-pageview",
            url: document.location.href,
            title: document.title
        })
    }
    function S(t) {
        window.siteAnalyticsUtil.sendToDataLayer({
            event: "site-language-change",
            language: t
        })
    }
    function E(t) {
        window.siteAnalyticsUtil.sendToDataLayer({
            event: "site-country-change",
            country: t
        })
    }
    function D() {
        window.siteAnalyticsUtil.sendToGoogleAnalytics(["send", "event", "Click", "Try Demo", "demo"])
    }
    window.siteAnalytics.trackBillingInfraDiagramPricingPlan = h,
    window.siteAnalytics.trackBillingInfraDiagramNode = v,
    window.siteAnalytics.trackBillingInteractiveInvoice = A,
    window.siteAnalytics.trackConnectExpressDemoCta = D,
    window.siteAnalytics.trackConnectRoutingDiagram = m,
    window.siteAnalytics.trackCountryChange = E,
    window.siteAnalytics.trackDocsPageView = L,
    window.siteAnalytics.trackHomePageNotebook = y,
    window.siteAnalytics.trackLanguageChange = S,
    window.siteAnalytics.trackRadarIcosahedron = f,
    window.siteAnalytics.trackRadarFraudChart = g,
    window.siteAnalytics.trackSigmaQueryCategory = b,
    window.siteAnalytics.trackSigmaQueryExample = k,
    window.siteAnalytics.trackSigmaPricingSlider = C
}(),
function() {
    var t = "inline_link"
      , e = "button"
      , n = "pdf_link";
    function i(i) {
        var o, a = function(i) {
            var o = i.className.toLowerCase()
              , a = i.getAttribute("href");
            return /\.pdf$|\.pdf#|\.pdf\?/i.test(i.href) ? n : -1 !== o.indexOf("button") || "#" === a ? e : t
        }(i), r = {
            text: (o = i.innerText,
            o.trim().replace(/\s+/g, " "))
        };
        window.siteAnalyticsUtil.emitAction(a, r)
    }
    function o(t) {
        if (window.siteAnalytics.hasAnalyticsAttributes(t.target))
            window.siteAnalytics.trackByAttributes(t.target);
        else {
            var e = function t(e) {
                return "A" === e.tagName ? e : e.parentNode ? t(e.parentNode) : null
            }(t.target);
            e && function(t) {
                return !!t.getAttribute("href")
            }(e) && i(e)
        }
    }
    document.addEventListener("click", o)
}(),
function() {
    function t() {
        if (function() {
            if (!document.documentElement.id)
                return !1;
            return !!window.siteAnalyticsUtil.siteAnalyticsConfig().trackPageViewed
        }()) {
            var t = window.siteAnalyticsUtil.generalAnalyticsConfig()
              , e = document.documentElement.id;
            window.siteAnalyticsUtil.emitViewed(e, t)
        }
    }
    window.siteAnalytics.pageLoadTracking || (window.siteAnalytics.pageLoadTracking = {
        trackPageView: t
    },
    window.addEventListener("load", t))
}(),
function() {
    var t = "video_expand"
      , e = "video_end"
      , n = "video_play";
    function i(e) {
        return r(t, e)
    }
    function o(t) {
        return r(n, t)
    }
    function a(t) {
        return r(e, t)
    }
    function r(t, e) {
        var n, i, o = e;
        "string" != typeof e && (n = function(t) {
            return t.currentSrc || t.getAttribute("src") || t.querySelector("source").getAttribute("src")
        }(e),
        o = (i = n.slice(n.lastIndexOf("/") + 1)).slice(0, i.lastIndexOf("."))),
        window.siteAnalyticsUtil.emitAction(t, {
            video: o
        })
    }
    function s(t) {
        "VIDEO" === t.target.tagName && o(t.target)
    }
    function c(t) {
        "VIDEO" === t.target.tagName && a(t.target)
    }
    document.addEventListener("play", s, !0),
    document.addEventListener("ended", c, !0),
    window.siteAnalytics.trackVideoExpand = i,
    window.siteAnalytics.trackVideoPlay = o,
    window.siteAnalytics.trackVideoEnd = a
}();
