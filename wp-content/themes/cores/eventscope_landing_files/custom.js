function kmpSearch(pattern, text) {
  if (pattern.length == 0)
    return 0; // Immediate match

  // Compute longest suffix-prefix table
  var lsp = [0]; // Base case
  for (var i = 1; i < pattern.length; i++) {
    var j = lsp[i - 1]; // Start by assuming we're extending the previous LSP
    while (j > 0 && pattern.charAt(i) != pattern.charAt(j))
      j = lsp[j - 1];
    if (pattern.charAt(i) == pattern.charAt(j))
      j++;
    lsp.push(j);
  }

  // Walk through text string
  var j = 0; // Number of chars matched in pattern
  for (var i = 0; i < text.length; i++) {
    while (j > 0 && text.charAt(i) != pattern.charAt(j))
      j = lsp[j - 1]; // Fall back in the pattern
    if (text.charAt(i) == pattern.charAt(j)) {
      j++; // Next char matched, increment position
      if (j == pattern.length)
        return i - (j - 1);
    }
  }
  return -1; // Not found
}

function shuffle(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}


jQuery(document).ready(function($) {	



	$('.select-all').click(function () {
		$(this).parent().nextAll('.pill-tab:first').find('input[type="checkbox"]').prop('checked', true);
	});

	$('.deselect-all').click(function () {
		$(this).parent().nextAll('.pill-tab:first').find('input[type="checkbox"]').prop('checked', false);
	});

	var project_title = '';

	$('.project-filter').mouseover(function(e) {
		var btn = $(this);
		var btnTitle = btn.attr('xlink:title');
		project_title = $("#projects-title").text();
		$("#projects-title").text(btnTitle);
	});

	$('.project-filter').mouseout(function(e) {
		$("#projects-title").text(project_title);
	});

	$('.project-filter').click(function(e) {
		e.preventDefault();

		var btn = $(this);

		console.log(btn.attr('id'));

		var classes = btn.attr('id').split('-');
		var btnTitle = btn.attr('xlink:title');

		var form_arr = [];

		if(btn.attr('id') !== 'all'){
			for (var i = 0; i < classes.length; i++) {
				form_arr.push({name: "research_areas[]",  value: classes[i]});
			}
		}

		form_arr.push({name: "action", value: "filter_projects"});

		console.log(btn.attr('action'));
		console.log(form_arr);

		$.ajax({
	 			type: "post",
	 			url: btn.attr('action'),
	 			data: form_arr,
	 			dataType: "json",
	 			error: function(returnval) {
	 				console.log("error");
	 				//$("#publications_filter_form").find(":submit").removeClass("loading");
	 			},
	 			success: function (returnval) {
	 				console.log(returnval);

	 				$("#projects-results").empty();

	 				var html = '';

	 				if(returnval['return'].length === 0){
	 					html += '<div class="ui red message center-self">That didn\'t match any projects</div>';
	 				}
	 				else{
	 					for(k in returnval['return']){
			 				var proj = returnval['return'][k];
			 				html += '<a class="card" href="'+ proj['url'] +'">';

			 				if(proj['thumbnail']){
			 					html += '<div class="image">';
				 				html += '<img src="'+proj['thumbnail']+'">';
				 				html += '</div>';
			 				}

			 				html += '<div class="content">';
			 				html += '<h4 class="header">'+proj['name']+'</h4>';
			 				html += '</div>';
			 				html += '<div class="extra content"><i class="fas fa-file-alt icon"></i>';
			 				html += proj['num_pub']+' publication(s)';
			 				html += '</div>';
			 				html += '</a>';
			 			}
	 				}

		 			$("#projects-results").html(html);
		 			project_title = btnTitle;
		 			$("#projects-title").text(btnTitle);

	        }
	    });

	});

	$('#publications_filter_form').on('submit', function(e) {
		e.preventDefault();

		$("#publications_filter_form").find(":submit").addClass("loading");

		var $form = $(this);

 		console.log($form.serializeArray());

 		console.log($form.attr('action'));

 		$.ajax({
	 			type: "post",
	 			url: $form.attr('action'),
	 			data: $form.serialize(),
	 			dataType: "json",
	 			error: function(returnval) {
	 				console.log("error");
	 				$("#publications_filter_form").find(":submit").removeClass("loading");
	 			},
	 			success: function (returnval) {
	 				console.log(returnval);
	 				$("#publications-results").empty();

	 				var html = '';

	 				if(returnval['return'].length == 0){
	 					html += '<div class="ui red message">The active filters didn\'t match any publications</div>';
	 				}
	 				else{
	 					html += '<div class="ui styled fluid accordion">';
	 					colors = ['teal', 'blue', 'green', 'violet'];
		 				for(i in returnval['years']){
		 					html+= '<div class="active title"><i class="dropdown icon"></i>'+returnval['years'][i]+'</div><div class="active content"><div class="accordion">';
		 					//html += '<h3>'+returnval['years'][i]+'</h3>';
		 					for(j in returnval['venues']){
		 						html_ul = '';
		 						for(k in returnval['return']){
		 							var pub = returnval['return'][k];
		 							if(pub['year'] == returnval['years'][i] && pub['venue_type'] == returnval['venues'][j]){

		 								html_ul += pub['innerHTML'];

		 								// var startOfName = kmpSearch(pub['name'], pub['display_format']);
				 						// var authors_string = pub['display_format'].slice(0, startOfName);
				 						// var authors = authors_string.split(/[,]|and/);

				 						// console.log(authors);

				 						// for(ind in authors){
				 						// 	author = authors[ind];
				 						// 	if(/[\w]/.test(author)){
				 						// 		//console.log(author);
					 					// 		authors_string = authors_string.replace(author.trim(), '<a>'+author.trim()+'</a>');
					 					// 	}
				 						// }


				 						// html_ul += '<div class="ui fluid card" style="margin-bottom: 0.5em;"><div class="content"><div class="description">'+
				 						// 			authors_string+'<a class="boldref" target="_blank" href="'+pub['url']+'">'+pub['name']+'</a>'+pub['display_format'].slice(startOfName+pub['name'].length)
				 						// 			+'</div></div>';
				 						
				 						// html_tags = '';
				 						// shuffle(colors);
				 						// for(h in pub['tags']){
				 						// 	html_tags += '<div class="ui '+colors[h%colors.length]+' label">'+pub['tags'][h]['name']+'</div>';
				 						// }

				 						// if(html_tags !== ''){
				 						// 	html_ul += '<div class="extra content">';
				 						// 	html_ul +=  html_tags;
				 						// 	html_ul += '</div>';
				 						// }

				 						// html_ul += '</div>';
		 							}
				 				}
				 				if(html_ul !== ''){
		 							html += '<div class="active title"><i class="dropdown icon"></i>' + returnval['venues'][j] + '</div>';
		 							html += '<div class="active content">' + html_ul + '</div>';
				 				}
		 					}
		 					html += '</div></div>';
		 				}

		 				html += '</div>';
	 				}

	 				$("#publications-results").html(html);

	 				$('.ui.accordion').accordion({exclusive: false});

	 				$("#publications_filter_form").find(":submit").removeClass("loading");

	 				// for(i in returnval['return']){
	 				// 	var pub = returnval['return'][i];
	 				// 	var startOfName = kmpSearch(pub['name'], pub['display_format']);
	 				// 	//$("#publications-results").append('<li>'+pub['display_format'].slice(0, startOfName)+'</li>');
	 				// 	$("#publications-results").append('<li>'+pub['display_format'].slice(0, startOfName)+'<a target="_blank" href="'+pub['url']+'">'+pub['name']+'</a>'+pub['display_format'].slice(startOfName+pub['name'].length)+'</li>');
	 				// }
	 				// $("#publications_filter_form").find(":submit").removeClass("loading");
	            //setTimeout( function() { top.location.href="view.php" }, 3000 );  
	        }
	    });

 		// $.post($form.attr('action'), $form.serialize(), function(data) {
 		// 	console.log(data);
 		// }, 'json');
 	});


 	$('#publications_filter_form').submit();

 	$('.project-filter#all').click();

});