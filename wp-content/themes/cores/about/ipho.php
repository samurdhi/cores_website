<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>International Physics Olympiad (IPhO)</h3>
        </div>

        <div class="canvas-body">
            <div class="row">
                <p class="para">The International Physics Olympiad (IPhO) is an annual physics competition for high school students. It is one of the International Science Olympiads. The first IPhO was held in Warsaw, Poland in 1967.
                </p>
                <p class="para">Each national delegation is made up of at most five student competitors plus two leaders, selected on a national level. Observers may also accompany a national team. The students compete as individuals, and must sit for intensive theoretical and laboratory examinations. For their efforts the students can be awarded gold, silver, or bronze medals or an honourable mention.</p>
                <p class="para">The theoretical examination lasts 5 hours and consists of three questions. Usually these questions involve more than one part. The practical examination may consist of one laboratory examination of five hours, or two, which together take up the full five hours.</p>
            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>