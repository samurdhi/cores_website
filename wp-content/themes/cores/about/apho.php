<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>Asian Physics Olympiad (APhO)</h3>
        </div>

        <div class="canvas-body">
            <div class="row">
                <p class="para">
                    The Asian Physics Olympiad (APhO) is an annual physics competition for high school students from Asia and Oceania regions. It is one of the International Science Olympiads. The first APhO was hosted by Indonesia in 2000.

                    

                    
                </p>
                <p class="para">APhO has its origins in the International Physics Olympiad and is conducted according to similar statutes (One five-hour theoretical examination and one or two laboratory examinations). It is usually held about two months before the IPhO and can also be seen as additional training for the teams.</p>
                <p class="para">Each national delegation is made up of eight competitors (unlike five in the IPhO) plus two leaders. Observers may also accompany a national team. The leaders are involved in the selection, preparation and translation of the exam tasks, and the translation and marking of exam papers. The students compete as individuals, and must sit through intensive theoretical and laboratory examinations. For their efforts the students can be awarded a medal (gold, silver or bronze) or an honorable mention.</p>
            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>