<?php
/**
 * Template Name: News
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage CORES
 */

?>

<?php get_header(); ?>

<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>CORES News</h3>
        </div>

        <div class="canvas-body news-array">
           
           <div class="news-array">
               <div class="row">

               	<?php query_posts('cat=2'); ?>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<div class="small-12 medium-12 large-6 columns">
	                    <a class="ui fluid card news-item" href="<?php the_permalink();?>">
	                        <div class="image" style="background: black;">
	                            <img src="<?php the_post_thumbnail_url();?>" style="margin: 0 auto; width: unset;  border-radius: 0;">
	                        </div>
	                        <div class="content">
	                            <h4 class="header"><?php the_title(); ?></h4>
	                        </div>
	                    </a>
	                </div>
				<?php endwhile; endif; ?>

                

            </div>
            
           </div>
            
        </div>

    </div>

</div>

<?php get_footer(); ?>