<?php
/**
 * Template Name: People
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage CORES
 */

?>

<?php get_header(); ?>

<div class="row" style="margin-top: 50px;">

	<div class="ui fluid segment canvas">

		<div class="canvas-title">
			<h3><?php the_title();?></h3>
		</div>

		<div class="canvas-body">

			<?php $content = get_the_content();?>

			<h2>Faculty</h2>
			
			<?php $user_query = new WP_User_Query( array( 'meta_key' => 'lab_role', 'meta_value' => 'Faculty' ) );?>
			<div>
				<div class="ui four doubling cards">
					<?php if ( ! empty( $user_query->get_results() ) ): 
						foreach ( $user_query->get_results() as $user ):
							$variable = get_field('user_profile_page', "user_{$user->ID}");
							$variable1 = get_wp_user_avatar_src($user->ID);?>
							<a class="ui card" href="<?php echo $variable; ?>">
								<div class="image">
									<img src="<?php echo $variable1;?>">
								</div>
								<div class="content">
									<div class="header"><?php echo $user->display_name;?></div>
									<div class="meta">
										Principal Investigator
									</div>
								</div>
							</a>
					<?php endforeach; endif;?>
				</div>
			</div>

			<h2 style="margin-top: 40px;">PhD Students</h2>
			<?php $user_query = new WP_User_Query( array( 'meta_key' => 'lab_role', 'meta_value' => 'PhD Student' ) );?>
			<div>
				<div class="ui four doubling cards">
					<?php if ( ! empty( $user_query->get_results() ) ): 
						foreach ( $user_query->get_results() as $user ):
							$variable = get_field('user_profile_page', "user_{$user->ID}");
							$variable1 = get_wp_user_avatar_src($user->ID);?>
							<a class="ui card" href="<?php echo $variable; ?>">
								<div class="image">
									<img src="<?php echo $variable1;?>">
								</div>
								<div class="content">
									<div class="header"><?php echo $user->display_name;?></div>
									<div class="meta">
										PhD student
									</div>
								</div>
							</a>
					<?php endforeach; endif;?>
				</div>
			</div> 

<!-- 			<div class="ui four doubling cards">
				<a class="ui card" href="#">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/avatar/han-thumbnail.jpg">
					</div>
					<div class="content">
						<div class="header">Han Yan</div>
						<div class="meta">
							PhD student
						</div>
					</div>
				</a>
				<a class="ui card" href="#">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/avatar/veljko-thumbnail.jpg">
					</div>
					<div class="content">
						<div class="header">Veljko Boljanovic</div>
						<div class="meta">
							PhD student
						</div>
					</div>
				</a>
				<a class="ui card" href="#">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/avatar/samer.jpg">
					</div>
					<div class="content">
						<div class="header">Samer Hanna</div>
						<div class="meta">
							PhD student
						</div>
					</div>
				</a>
				<a class="ui card" href="#">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/avatar/enes.jpg">
					</div>
					<div class="content">
						<div class="header">Enes Krijestorac</div>
						<div class="meta">
							PhD student
						</div>
					</div>
				</a>
				<a class="ui card" href="/people/samurdhi-karunaratne">
					<div class="image">
						<img src="http://localhost/wp-content/uploads/2020/08/avatar1597418925.png">
					</div>
					<div class="content">
						<div class="header">Samurdhi Karunaratne</div>
						<div class="meta">
							MS/PhD student
						</div>
					</div>
				</a>
				<a class="ui card" href="#">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/avatar/ben-thumbnail.jpg">
					</div>
					<div class="content">
						<div class="header">Benjamin Domae</div>
						<div class="meta">
							MS/PhD student
						</div>
					</div>
				</a>
			</div> -->

<!-- 			<h2 style="margin-top: 40px;">Visitors</h2>

			<div class="ui four doubling cards">
				<a class="ui card" href="#">
					<div class="image">
						<img src="<?php echo get_template_directory_uri();?>/assets/avatar/agon-thumbnail.jpg">
					</div>
					<div class="content">
						<div class="header">Agon Memedi</div>
						<div class="meta">
							Visitor
						</div>
					</div>
				</a>

			</div> -->

			<div style="margin-top: 40px;"></div>

			<?php echo $content;?>

		</div>

		</div>

	</div>

</div>

<?php get_footer(); ?>