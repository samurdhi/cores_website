<?php
/**
 * Template Name: Research
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage CORES
 */

?>

<?php get_header(); ?>

<div class="row" style="margin-top: 50px;">

	<div class="ui fluid segment canvas">

		<div class="canvas-title">
			<h3>Research</h3>
		</div>

		<div class="canvas-body news-array">

			<?php $content = get_the_content();?>

			

			<?php echo $content;?>

			<div id="aspect-content">


				<?php

				$posts = get_posts(array(
					'post_type' => 'research_area'
				));

				 $color_arr = array('#82d428', '#ff6e00', '#2185d0');

				foreach ($posts as $i => $post) { 
					setup_postdata($post); $mypod = pods('research_area', $post->ID);
					$ra_permalink=get_the_permalink();
					?>


					<div class="aspect-tab " style="background-color: <?php echo $mypod->field('research_area_color').'1f'; ?>">
						<input id="item-<?php echo $i;?>" type="checkbox" class="aspect-input" name="aspect">
						<label for="item-<?php echo $i;?>" class="aspect-label"></label>
						<div class="aspect-content">
							<div class="aspect-info">
									<img width="50px" height="50px" src="/wp-content/themes/cores/assets/<?php echo $mypod->field('research_area_slug'); ?>.svg">
								<span class="aspect-name"><?php the_title(); ?></span>
							</div>
							
						</div>
						<div class="aspect-tab-content" style="background-color: <?php echo $mypod->field('research_area_color').'1f'; ?>">
							<div class="sentiment-wrapper">
								<?php echo $mypod->field('research_area_short_description');?>

								<h4 class="ui header">Selected Projects</h4>

								<?php

								$projects = pods('research', array("where" => array(
									array(
										"field"   => "project_research_area.id",
										"value"   => $post->ID,
										"compare" => "="
									)
								)));


								$project_ids = array();

								while ( $projects->fetch() ) {
									$project_ids[] = $projects->id();
								}

								$pubs = pods('publication', array("where" => array(
									array(
										"field"   => "pub_project.id",
										"value"   => $project_ids,
										"compare" => "="
									),
								)));

								?>



								<?php

								//$posts = pods('research', array("limit"=> -1));

								$projects = pods('research', array("where" => array(
									array(
										"field"   => "project_research_area.id",
										"value"   => $post->ID,
										"compare" => "="
									)
								)));

								while ($projects->fetch()) {
									$pic = $projects->field('project_thumbnail');
									error_log($pic['ID']);
									$curr_pubs = pods('publication', array("where" => array(
										array(
											"field"   => "pub_project.id",
											"value"   => $projects->id(),
											"compare" => "="
										),
									))); ?>

									<div class="ui segment"						>
										<div class="ui items">
											<div class="item" style="margin: 0 !important;">
												<?php if(wp_get_attachment_url($pic['ID'])): ?>
												<div class="image">
													<img style="width: 100%;" src="<?php echo wp_get_attachment_url($pic['ID']);?>">
												</div>
												<?php endif;?>
												<div class="content">
													<a target="_blank" href="<?php echo get_permalink($projects->id());?>" class="header"><?php echo $projects->display('project_name'); ?></a>
													<div class="meta">
														<span><i class="fas fa-file-alt icon"></i>
												<?php echo $curr_pubs->total();?> publication(s)</span>
													</div>
													<div class="description">
														<p><?php //the_excerpt(); ?></p>
													</div>
												</div>
											</div>
										</div>
									</div>

								<?php }?>


								<h4 class="ui header">Recent Publications</h4>

								<?php

										//$project_ids = array();



								$iter =0;

								while($pubs->fetch()){
									if($iter>=3){
										break;
									}
									$curr_pub = [];
									        //error_log($pubs->display('pub_display_format'), 3, "../my-errors.log");
									$curr_pub['name'] = $pubs->display('pub_name');
									$years[] = $pubs->display('pub_year');
									$venues[] = $pubs->display('pub_venue_type');
									$curr_pub['year'] = $pubs->display('pub_year');
									$curr_pub['venue_type'] = $pubs->display('pub_venue_type');
									$curr_pub['tags'] = $pubs->field('pub_publication_tags');
									$curr_pub['url'] = $pubs->field('pub_url');
									$curr_pub['display_format'] = $pubs->display('pub_display_format');
									echo processPublication($curr_pub['display_format'], $pubs->display('pub_name'), $pubs->field('pub_url'), $pubs->field('pub_publication_tags'));
									$iter=$iter+1;
								}

										// while ( $pubs->fetch() ) {
										// 	echo $pubs->display('pub_name').'<br>';
										// }

								?>

								<div class="small-12 medium-10 large-12 align-center columns" style="margin-top: 20px;">
									<a class="ui grey button" href="<?php echo $ra_permalink;?>"><i class="fas fa-location-arrow icon"></i> See research area  </a>
								</div>

							</div>
						</div>
					</div>

				<?php }?>
			</div>

			<!-- <div class="small-12 medium-10 large-12 align-center columns" style="margin-top: 30px;">
				<a class="ui blue button" href="/projects" style="margin-top: 10px;" ><i class="fas fa-location-arrow icon"></i> See current projects  </a>
				<a class="ui green button" href="/publications" style="margin-top: 10px;" ><i class="fas fa-file-alt icon"></i> See publications  </a>
			</div> -->

			<div class="align-center" style="margin-top: 30px;">
				<h4 class="ui header center-self">Click on different parts of the Venn Diagram below to filter projects by associated research area(s)</h4>
			</div>

			<div style="    width: 60%;
    margin: 30px auto 20px;">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 589 512"><defs><style>.cls-1{fill:#ffd3a4;}.cls-2{fill:#ff931e;}.cls-3{fill:#e4deac;}.cls-4{fill:#c9e9b3;}.cls-5{fill:#7ac943;}.cls-6{fill:#d0ddc3;}.cls-7{fill:#7c9f5a;}.cls-8{fill:#d4d7cc;}.cls-9{fill:#b9e3d3;}.cls-10{fill:#a9ddf3;}.cls-11{fill:#29abe2;}.cls-12{fill:#999;}.cls-13{fill:#009245;}.cls-14,.cls-17{fill:none;stroke-miterlimit:10;}.cls-14{stroke:#ff931e;stroke-linecap:round;stroke-width:10px;}.cls-15,.cls-16,.cls-17{isolation:isolate;}.cls-16,.cls-17{font-size:58.38px;font-family:MicrosoftPhagsPa, Microsoft PhagsPa;}.cls-17{stroke:#000;}.cls-18{fill:#0071bc;}.cls-19{fill:#3fa9f5;}</style></defs><title>Asset 12</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><a  class="project-filter" id="mmwave" action="<?php echo admin_url('admin-ajax.php'); ?>" xlink:title="5G mmWave comm. projects"><path class="cls-1" d="M163.53,327.21C91.64,317.6,37.5,255.7,37.5,183c0-80.23,65.27-145.5,145.5-145.5A145.14,145.14,0,0,1,294.85,89.94,146.64,146.64,0,0,0,261.5,183c0,2.94.09,5.84.26,8.63A146.9,146.9,0,0,0,163.53,327.21Z"/></a><path class="cls-2" d="M183,38A144.64,144.64,0,0,1,294.2,89.94,147.15,147.15,0,0,0,261,183c0,2.81.08,5.59.24,8.29A147.39,147.39,0,0,0,163,326.64,145,145,0,0,1,183,38m0-1a146,146,0,0,0-19,290.78A146.06,146.06,0,0,1,262.28,192q-.28-4.46-.28-9a145.35,145.35,0,0,1,33.5-93.06A145.69,145.69,0,0,0,183,37Z"/><a class="project-filter" id="mmwave-iot" action="<?php echo admin_url('admin-ajax.php'); ?>" xlink:title="5G mmWave Projects and IoT projects" ><path class="cls-3" d="M329,183c0,.74,0,1.48,0,2.22A148.55,148.55,0,0,0,310,184a145.76,145.76,0,0,0-47.72,8q-.28-4.46-.28-9a145.35,145.35,0,0,1,33.5-93.06A145.35,145.35,0,0,1,329,183Z"/></a><a class="project-filter" id="iot" action="<?php echo admin_url('admin-ajax.php'); ?>" xlink:title="Distributed Comms. and Sensing for IoT projects" ><path class="cls-4" d="M456.18,320.33A146.51,146.51,0,0,0,329.49,184.78c0-.59,0-1.19,0-1.78a146.64,146.64,0,0,0-33.35-93.06A145.14,145.14,0,0,1,408,37.5c80.23,0,145.5,65.27,145.5,145.5A145.58,145.58,0,0,1,456.18,320.33Z"/></a><path class="cls-5" d="M408,38a145,145,0,0,1,48.63,281.64A147,147,0,0,0,330,184.35c0-.45,0-.9,0-1.35a147.15,147.15,0,0,0-33.2-93.06A144.64,144.64,0,0,1,408,38m0-1A145.69,145.69,0,0,0,295.5,89.94,145.35,145.35,0,0,1,329,183c0,.74,0,1.48,0,2.22A146,146,0,0,1,455.72,321,146,146,0,0,0,408,37Z"/><a class="project-filter" id="mmwave-iot-ml" action="<?php echo admin_url('admin-ajax.php'); ?>" xlink:title="5G mmWave, IoT and ML projects" ><path class="cls-6" d="M295.5,275.27a144.07,144.07,0,0,1-32.7-82.94A145.05,145.05,0,0,1,310,184.5a148.46,148.46,0,0,1,18.47,1.16A145.73,145.73,0,0,1,295.5,275.27Z"/></a><path class="cls-7" d="M310,185a148,148,0,0,1,18,1.1,144.18,144.18,0,0,1-32.46,88.38,143.59,143.59,0,0,1-32.17-81.8A144.52,144.52,0,0,1,310,185m0-1a145.76,145.76,0,0,0-47.72,8,145.22,145.22,0,0,0,33.22,84.08A145.31,145.31,0,0,0,329,185.22,148.55,148.55,0,0,0,310,184Z"/><a class="project-filter" id="mmwave-ml" action="<?php echo admin_url('admin-ajax.php'); ?>" xlink:title="5G mmWave projects and ML projects" ><path class="cls-8" d="M295.5,276.06A145.69,145.69,0,0,1,183,329a148.55,148.55,0,0,1-19-1.22A146.06,146.06,0,0,1,262.28,192,145.22,145.22,0,0,0,295.5,276.06Z"/></a><a class="project-filter" id="ml-iot" action="<?php echo admin_url('admin-ajax.php'); ?>" xlink:title="IoT and ML projects"><path class="cls-9" d="M455.72,321a146.18,146.18,0,0,1-160.22-45A145.31,145.31,0,0,0,329,185.22,146,146,0,0,1,455.72,321Z"/></a><a class="project-filter" id="ml" action="<?php echo admin_url('admin-ajax.php'); ?>" xlink:title="Machine Learning for Wireless Networks Co-existence and Security projects" ><path class="cls-10" d="M310,475.5c-80.23,0-145.5-65.27-145.5-145.5,0-.55,0-1.1,0-1.65A150.71,150.71,0,0,0,183,329.5a146.12,146.12,0,0,0,112.5-52.66,146.7,146.7,0,0,0,159.76,44.87c.16,2.69.24,5.47.24,8.29C455.5,410.23,390.23,475.5,310,475.5Z"/></a><path class="cls-11" d="M295.5,277.62a147.19,147.19,0,0,0,159.3,44.77c.13,2.48.2,5,.2,7.61a145,145,0,0,1-290,0c0-.36,0-.73,0-1.09A150.54,150.54,0,0,0,183,330a146.61,146.61,0,0,0,112.5-52.38m0-1.56A145.69,145.69,0,0,1,183,329a148.55,148.55,0,0,1-19-1.22c0,.74,0,1.48,0,2.22a146,146,0,0,0,292,0q0-4.53-.28-9a146.18,146.18,0,0,1-160.22-45Z"/><a class="project-filter" id="all" action="<?php echo admin_url('admin-ajax.php'); ?>" xlink:title="All projects" ><path class="cls-12" d="M568,21V491H21V21H568M589,0H0V512H589V0Z"/></a><g id="Layer_1-2-2" data-name="Layer 1-2"><path class="cls-5" d="M477.75,177.49a9.23,9.23,0,0,0-8.88,6.72H460v-9.84h9.11a17.93,17.93,0,0,0,5.2-35.1,20.48,20.48,0,0,0-30.12-12.76,17.94,17.94,0,0,0-30,12.65,17.93,17.93,0,0,0,4.82,35.21H428v9.84h-8.91a9.23,9.23,0,1,0,0,5h11.43a2.52,2.52,0,0,0,2.52-2.52V174.37h8.4v15.22a9.23,9.23,0,1,0,5,0V174.37h8.39v12.35a2.52,2.52,0,0,0,2.52,2.52h11.44a9.23,9.23,0,1,0,8.88-11.75Zm-67.51,13.43a4.2,4.2,0,1,1,4.2-4.2h0A4.2,4.2,0,0,1,410.24,190.92Zm38,7.56a4.2,4.2,0,1,1-4.2-4.2h0A4.2,4.2,0,0,1,448.2,198.48Zm-29.26-29.15a12.89,12.89,0,0,1-1.88-25.65,2.52,2.52,0,0,0,2.15-2.73,11.71,11.71,0,0,1-.06-1.19,12.9,12.9,0,0,1,22.71-8.37,2.52,2.52,0,0,0,3.42.39,15.21,15.21,0,0,1,9.13-3,15.47,15.47,0,0,1,15.23,12.93,2.51,2.51,0,0,0,2,2.06,12.9,12.9,0,0,1-2.56,25.53Zm58.81,21.59a4.2,4.2,0,1,1,4.2-4.2h0A4.2,4.2,0,0,1,477.75,190.92Z"/><circle class="cls-13" cx="443.75" cy="161.38" r="4.51"/><path class="cls-13" d="M454.54,150.51l-.21-.2-.2-.19L454,150a14.73,14.73,0,0,0-20.34.48l-.65.65-.9.9a2.83,2.83,0,0,0,4,4h0l1.56-1.56a9.09,9.09,0,0,1,12.84,0l1.13,1.09a2.81,2.81,0,0,0,4-4Z"/><path class="cls-13" d="M462.23,144.93l-.93-.94s0,0,0,0l-.74-.74a2.72,2.72,0,0,0-.42-.34,24.58,24.58,0,0,0-33.19.76l-.14.11-1.69,1.7a2.58,2.58,0,0,0,3.65,3.65l1.7-1.69,0,0a19.39,19.39,0,0,1,26.76-.15l1.33,1.34a2.59,2.59,0,0,0,3.66-3.66Z"/></g><g id="Layer_1-2-3" data-name="Layer 1-2"><path class="cls-14" d="M221,194.5a64.07,64.07,0,0,0-64-64"/><path class="cls-14" d="M157,150.5a44.05,44.05,0,0,1,44,44"/><path class="cls-14" d="M201,194.5a44.05,44.05,0,0,0-44-44"/><path class="cls-14" d="M157,170.5a24,24,0,0,1,24,24"/><path class="cls-14" d="M181,194.5a24,24,0,0,0-24-24"/><g class="cls-15"><path class="cls-2" d="M109,190v7.45a3.79,3.79,0,0,1-.8,2.64,2.69,2.69,0,0,1-2.11.88,2.63,2.63,0,0,1-2.06-.88,3.84,3.84,0,0,1-.79-2.64v-8.93a30.29,30.29,0,0,0-.14-3.28,3.6,3.6,0,0,0-.78-1.91,2.51,2.51,0,0,0-2-.75c-1.83,0-3,.63-3.62,1.88a13.38,13.38,0,0,0-.87,5.42v7.57a3.88,3.88,0,0,1-.79,2.63,2.63,2.63,0,0,1-2.08.89,2.68,2.68,0,0,1-2.09-.89,3.78,3.78,0,0,1-.81-2.63v-16a3.55,3.55,0,0,1,.73-2.4,2.45,2.45,0,0,1,1.92-.82,2.64,2.64,0,0,1,1.92.77,2.87,2.87,0,0,1,.77,2.12v.53A9.85,9.85,0,0,1,98.51,179a8.19,8.19,0,0,1,3.69-.82,7.38,7.38,0,0,1,3.63.84,6.59,6.59,0,0,1,2.51,2.54,9.73,9.73,0,0,1,3-2.55,7.81,7.81,0,0,1,3.59-.83,8.21,8.21,0,0,1,4,.9,5.75,5.75,0,0,1,2.48,2.59,11.77,11.77,0,0,1,.72,4.77v10.94a3.84,3.84,0,0,1-.8,2.64,2.72,2.72,0,0,1-2.11.88,2.68,2.68,0,0,1-2.09-.89,3.78,3.78,0,0,1-.81-2.63V188a22.17,22.17,0,0,0-.15-2.89,3.3,3.3,0,0,0-.83-1.82,2.63,2.63,0,0,0-2-.74,3.79,3.79,0,0,0-2.1.65,4.44,4.44,0,0,0-1.55,1.77A14,14,0,0,0,109,190Z"/><path class="cls-2" d="M146.17,190v7.45a3.79,3.79,0,0,1-.8,2.64,2.69,2.69,0,0,1-2.11.88,2.63,2.63,0,0,1-2.06-.88,3.84,3.84,0,0,1-.79-2.64v-8.93a30.29,30.29,0,0,0-.14-3.28,3.6,3.6,0,0,0-.78-1.91,2.51,2.51,0,0,0-2-.75c-1.83,0-3,.63-3.62,1.88a13.38,13.38,0,0,0-.87,5.42v7.57a3.88,3.88,0,0,1-.79,2.63,2.63,2.63,0,0,1-2.08.89,2.68,2.68,0,0,1-2.09-.89,3.78,3.78,0,0,1-.81-2.63v-16A3.55,3.55,0,0,1,128,179a2.45,2.45,0,0,1,1.92-.82,2.64,2.64,0,0,1,1.92.77,2.87,2.87,0,0,1,.77,2.12v.53a9.85,9.85,0,0,1,3.11-2.56,8.19,8.19,0,0,1,3.69-.82,7.38,7.38,0,0,1,3.63.84,6.59,6.59,0,0,1,2.51,2.54,9.73,9.73,0,0,1,3-2.55,7.81,7.81,0,0,1,3.59-.83,8.21,8.21,0,0,1,4,.9,5.75,5.75,0,0,1,2.48,2.59,11.77,11.77,0,0,1,.72,4.77v10.94a3.84,3.84,0,0,1-.8,2.64,2.72,2.72,0,0,1-2.11.88,2.68,2.68,0,0,1-2.09-.89,3.78,3.78,0,0,1-.81-2.63V188a22.17,22.17,0,0,0-.15-2.89,3.3,3.3,0,0,0-.83-1.82,2.63,2.63,0,0,0-2.05-.74,3.79,3.79,0,0,0-2.1.65,4.44,4.44,0,0,0-1.55,1.77A14,14,0,0,0,146.17,190Z"/></g><text class="cls-16" transform="translate(87.5 170.13) scale(0.87 1)">5G</text><text class="cls-17" transform="translate(87.5 170.13) scale(0.87 1)">5G</text></g><g id="Layer_1-2-4" data-name="Layer 1-2"><path class="cls-18" d="M310.42,403.25a.83.83,0,1,0,.82.83A.83.83,0,0,0,310.42,403.25Z"/><path class="cls-18" d="M302.22,403.25a.83.83,0,1,0,.83.83A.83.83,0,0,0,302.22,403.25Z"/><path class="cls-18" d="M324.11,421a6.57,6.57,0,0,0,1.06-3.52,5.71,5.71,0,0,0-2.07-4.4,5.53,5.53,0,0,0,.41-2.16,6.07,6.07,0,0,0-3.31-5.38,4.93,4.93,0,0,0-3.73-5.37,5.91,5.91,0,0,0-5.25-3.66,5.78,5.78,0,0,0-4.92,3,5.78,5.78,0,0,0-4.92-3,5.91,5.91,0,0,0-5.25,3.66,4.93,4.93,0,0,0-3.73,5.37,6.07,6.07,0,0,0-3.31,5.38,5.55,5.55,0,0,0,.43,2.16,5.71,5.71,0,0,0-2.07,4.4,6.57,6.57,0,0,0,1.06,3.52,5.65,5.65,0,0,0,.66,7.36,6,6,0,0,0,5.67,6.85h.08a6.47,6.47,0,0,0,11.4-.29,6.47,6.47,0,0,0,11.4.29h.08a5.94,5.94,0,0,0,5.73-6,4.91,4.91,0,0,0-.07-.86,5.65,5.65,0,0,0,.66-7.35Zm-18.61-12.1a3.24,3.24,0,0,0-1.66-.43.82.82,0,0,0-.95.68.83.83,0,0,0,.69.95.57.57,0,0,0,.26,0,1.66,1.66,0,0,1,1.66,1.66V427.3a6.55,6.55,0,0,0-4.92-2.23.83.83,0,0,0-.26,1.64,1.13,1.13,0,0,0,.26,0,4.93,4.93,0,0,1,4.92,4.91,5.11,5.11,0,0,1-4.92,5.17,4.93,4.93,0,0,1-4.45-2.86.83.83,0,0,0-.83-.47l-.18,0c-2.33.31-5-2.33-4.24-5.28a.84.84,0,0,0-.28-.83,4.09,4.09,0,0,1-1.49-3.14,4,4,0,0,1,.6-2.1,5.52,5.52,0,0,0,3.5,1.28.83.83,0,1,0,0-1.65,4.31,4.31,0,0,1-4.1-4.35A4.05,4.05,0,0,1,291,414a.84.84,0,0,0,.27-1.1,3.94,3.94,0,0,1-.55-2,4.11,4.11,0,1,1,8.2,0,.83.83,0,1,0,1.66,0,5.94,5.94,0,0,0-5.74-6,4.57,4.57,0,0,0-.83.07v-.07a3.28,3.28,0,0,1,5.6-2.32.83.83,0,0,0,1.35-1,1,1,0,0,0-.2-.2,4.84,4.84,0,0,0-2.72-1.38,4.14,4.14,0,0,1,3.34-1.94,4.33,4.33,0,0,1,4.1,4.34Zm13.93,14.57a5.59,5.59,0,0,0,3.5-1.29,4,4,0,0,1,.6,2.11,4.09,4.09,0,0,1-1.49,3.13.81.81,0,0,0-.27.83,3.83,3.83,0,0,1,.12,1c0,2.54-2.3,4.6-4.37,4.32l-.18,0a.85.85,0,0,0-.83.47,4.91,4.91,0,0,1-4.45,2.86,5.11,5.11,0,0,1-4.91-5.16,4.91,4.91,0,0,1,4.91-4.92A.83.83,0,0,0,313,426a.82.82,0,0,0-.68-.95h-.27a6.52,6.52,0,0,0-4.91,2.23v-15.6A1.65,1.65,0,0,1,308.8,410a.83.83,0,0,0,.26-1.64.57.57,0,0,0-.26,0,3.19,3.19,0,0,0-1.65.44v-6.43a4.33,4.33,0,0,1,4.09-4.34,4.13,4.13,0,0,1,3.34,1.94,4.8,4.8,0,0,0-2.71,1.39.82.82,0,0,0-.2,1.15.83.83,0,0,0,1.16.19.69.69,0,0,0,.19-.19,3.29,3.29,0,0,1,5.6,2.32v.07a4.57,4.57,0,0,0-.83-.07,6,6,0,0,0-5.74,6,.83.83,0,0,0,1.66,0,4.11,4.11,0,1,1,8.2,0,4,4,0,0,1-.55,2,.84.84,0,0,0,.27,1.1,4.08,4.08,0,0,1,1.92,3.46,4.31,4.31,0,0,1-4.1,4.34.83.83,0,0,0,0,1.66Z"/><path class="cls-18" d="M303.65,414a.83.83,0,0,0-1.16-.06,4.1,4.1,0,0,1-6.83-3.06.83.83,0,1,0-1.66,0,5.74,5.74,0,0,0,4.8,5.66,2.48,2.48,0,0,1-2.34,1.72.83.83,0,0,0,0,1.66,4.09,4.09,0,0,0,4-3.32,5.71,5.71,0,0,0,3.09-1.41.84.84,0,0,0,.1-1.17Z"/><path class="cls-18" d="M294.84,428.33a.84.84,0,0,0-.69.95.83.83,0,0,0,.69.69,4.34,4.34,0,0,1,4.1,4.35.83.83,0,0,0,1.66,0A6,6,0,0,0,294.84,428.33Z"/><path class="cls-18" d="M301.4,421.77A7.39,7.39,0,0,0,295,425.5a.84.84,0,0,0,.41,1.1.83.83,0,0,0,1-.27,5.74,5.74,0,0,1,5-2.9.83.83,0,1,0,0-1.66Z"/><path class="cls-18" d="M316.15,418.25a2.48,2.48,0,0,1-2.34-1.72,5.74,5.74,0,0,0,4.8-5.66.83.83,0,0,0-1.64-.26,1.13,1.13,0,0,0,0,.26,4.1,4.1,0,0,1-6.83,3.06.83.83,0,0,0-1.09,1.24,5.72,5.72,0,0,0,3.08,1.41,4.1,4.1,0,0,0,4,3.32.83.83,0,0,0,0-1.66Z"/><path class="cls-18" d="M317.8,428.33a5.94,5.94,0,0,0-5.74,6,.83.83,0,1,0,1.65,0,4.34,4.34,0,0,1,4.11-4.35.83.83,0,0,0,0-1.64Z"/><path class="cls-18" d="M317.65,425.51a7.37,7.37,0,0,0-6.41-3.73.83.83,0,0,0,0,1.66,5.73,5.73,0,0,1,5,2.9.83.83,0,0,0,1.42-.83Z"/><path class="cls-19" d="M306.94,375.49a33.73,33.73,0,0,0-24,9.95,6.45,6.45,0,0,0,9.12,9.11,21.08,21.08,0,0,1,29.81,0h0a6.44,6.44,0,0,0,9.11-9.11A33.72,33.72,0,0,0,306.94,375.49Z"/><path class="cls-19" d="M306.94,354.27a54.81,54.81,0,0,0-39,16.16,6.45,6.45,0,0,0,9.11,9.12,42.31,42.31,0,0,1,59.82,0h0a6.45,6.45,0,0,0,9.12-9.12A54.85,54.85,0,0,0,306.94,354.27Z"/><path class="cls-19" d="M361,355.4a76.47,76.47,0,0,0-108.13,0h0a6.45,6.45,0,0,0,9.12,9.11,63.58,63.58,0,0,1,89.9,0,6.44,6.44,0,0,0,9.31-8.91l-.2-.2Z"/></g></g></g></svg>

			</div>

			<div class="align-center">
				<h3 class="ui header center-self" id="projects-title">All projects</h3>
			</div>

				<div id="projects-results" class="ui three stackable cards " style="margin-top: 10px;">

	        	</div>

	        <div class="small-12 medium-10 large-12 align-center columns" style="margin-top: 30px;">
				<a class="ui blue button" href="/archived-projects" style="margin-top: 10px;" ><i class="fas fa-location-arrow icon"></i> See archived projects  </a>
				<a class="ui green button" href="/publications" style="margin-top: 10px;" ><i class="fas fa-file-alt icon"></i> See publications  </a>
			</div>


		</div>

	</div>

</div>

<?php get_footer(); ?>