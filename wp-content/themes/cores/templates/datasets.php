<?php
/**
 * Template Name: Datasets
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage CORES
 */

?>

<?php get_header(); ?>

<div class="row" style="margin-top: 50px;">

	<div class="ui fluid segment canvas">

		<div class="canvas-title">
			<h3>Datasets</h3>
		</div>

		<div class="canvas-body news-array">

			<?php $content = get_the_content();?>

			

			<?php echo $content;?>


			<div id="projects-results" class="ui three cards " style="margin-top: 10px;">


				<?php

                $posts = get_posts(array(
                    'category' => get_cat_ID('Datasets'),
                    'numberposts' => -1
                ));

                foreach ($posts as $i => $post) {
                    setup_postdata($post); ?>

                    <a class="card" href="<?php the_permalink(); ?>">
						<?php if (has_post_thumbnail()) {?>
							<div class="image">
			 					<?php the_post_thumbnail(); ?>
			 				</div>
						<?php } ?>
		 				<div class="content">
		 					<h4 class="header"><?php the_title(); ?></h4>
		 					<div class="description">
						     <?php the_excerpt(); ?>
						    </div>
		 				</div>
	 				</a>

                <?php }?>

						

        	</div>


		</div>

	</div>

</div>

<?php get_footer(); ?>