<?php
/**
 * Template Name: Archived Projects
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage CORES
 */

?>

<?php get_header(); ?>

<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment canvas">

        <div class="canvas-title">
            <h3>Archived Projects</h3>
        </div>

        <div class="canvas-body news-array">

            <div class="news-array">
                <div class="row">


                    <?php

                    $posts = get_posts(array(
                        'post_type' => 'research',
                        'numberposts' => -1
                    ));

                    foreach ($posts as $i => $post) {
                        setup_postdata($post); $mypod = pods('research', $post->ID); $pic = $mypod->field('project_thumbnail'); $pubs = pods('publication', array("where" => array(
                            array(
                                "field"   => "pub_project.id",
                                "value"   => $post->ID,
                                "compare" => "="
                            ),
                        )));

                        if(!$mypod->field('project_archived')) continue;

                        ?>

                        <a class="ui fluid card" href="<?php the_permalink(); ?>">
                            <div class="image" style="background: black;">
                                <img src="<?php echo wp_get_attachment_url($pic['ID']);?>" style="margin: 0 auto; width: unset;">
                            </div>
                            <div class="content">
                                <h4 class="header"><?php the_title(); ?></h4>
                                <div class="meta">
                                    <span class="date">
                                        <strong>Principal Investigator(s): </strong><?php echo $mypod->display('project_staff_principal_investigators'); ?>
                                        <strong>        &nbsp;  &nbsp;  &nbsp;  &nbsp;Student(s): </strong><?php echo $mypod->display('project_staff_students'); ?>
                                    </span>
                                </div>
                                <div class="description">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                            <div class="extra content">
                                <i class="fas fa-file-alt icon"></i>
                                <?php echo $pubs->total();?> publication(s)
                            </div>
                        </a>

                    <?php }?>

<!--                                    <?php query_posts('cat=4'); ?>
                                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); $project_pi = get_field( "project_pi" ); $project_students = get_field( "project_students" ); ?>

                                                <a class="ui fluid card" href="<?php the_permalink();?>">
                                                        <div class="image" style="background: black;">
                                                                <img src="<?php the_post_thumbnail_url();?>" style="margin: 0 auto; width: unset;">
                                                        </div>
                                                        <div class="content">
                                                                <h4 class="header"><?php the_title(); ?></h4>
                                                                <div class="meta">
                                                              <span class="date">
                                                                <strong>Principal Investigator(s): </strong><?php echo $project_pi;?>
                                                                <strong>        &nbsp;  &nbsp;  &nbsp;  &nbsp;Student(s): </strong><?php echo $project_students;?>
                                                                  </span>
                                                            </div>
                                                                <div class="description">
                                                                        <?php the_excerpt(); ?>
                                                                </div>
                                                        </div>
                                                        <div class="extra content">
                                                                        <i class="fas fa-file-alt icon"></i>
                                                                        3 publications
                                                        </div>
                                                </a>
                                                <?php endwhile; endif; ?> -->



                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

<?php get_footer(); ?>