<?php
/**
 * Template Name: Contact Us
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage CORES
 */

?>

<?php get_header(); ?>

<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7Oy5ViJikt-5lBngHjosmMTF659hjSUQ&callback=initMap&libraries=&v=weekly"
      defer
    ></script>
<script>
  // Initialize and add the map
  function initMap() {
    // The location of Uluru
    const uluru = { lat: -25.344, lng: 131.036 };
    // The map, centered at Uluru
    const map = new google.maps.Map(document.getElementById("map"), {
      zoom: 4,
      center: uluru,
    });
    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({
      position: uluru,
      map: map,
    });
  }
</script>

<div class="row" style="margin-top: 50px;">

	<div class="ui fluid segment canvas">

		<div class="canvas-title">
			<h3><?php the_title();?></h3>
		</div>

		<div class="canvas-body">

			<?php the_content();?>
			
		</div>

		</div>

	</div>

</div>

<?php get_footer(); ?>