<?php
/**
 * Template Name: Downloads
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage CORES
 */

?>

<?php get_header(); ?>

<div class="row" style="margin-top: 50px;">

	<div class="ui fluid segment canvas">

		<div class="canvas-title">
			<h3>Downloads</h3>
		</div>

		<div class="canvas-body news-array">

		</div>

	</div>

</div>

<?php get_footer(); ?>