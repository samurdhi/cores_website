<?php
/**
 * Template Name: Publications
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage CORES
 */

?>

<?php get_header(); ?>

<div class="row" style="margin-top: 50px;">

	<div class="ui fluid segment canvas">

		<div class="canvas-title">
			<h3>Publications</h3>
		</div>

		<div class="canvas-body">

			<!-- <h3>Filter by</h3> -->

			<form class="ui form" id="publications_filter_form" method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
				<h4 class="ui dividing header">Year <button  type="button" class="ui mini green basic button select-all">Select all</button>
  												<button  type="button" class="ui mini red basic button deselect-all">Deselect all</button></h4>
				<div class="pill-tab">
					<div style="display: inline-block;">

						<?php
						$pubs = pods('publication',  array("limit"=> -1, "select" => 'pub_year.meta_value AS pub_year', 'orderby' => 'pub_year.meta_value DESC'));
										//print_r($pubs);
						while ( $pubs->fetch() ) {?>
							<label class="PillList-item">
								<input type="checkbox" name="pub_year[]" value="<?php echo $pubs->display( 'pub_year' );?>">
								<span class="PillList-label">
									<?php echo $pubs->display( 'pub_year' );?>
									<span class="Icon Icon--checkLight Icon--smallest"><i class="fa fa-check"></i></span>
								</span>
							</label>
						<?php }?>
					</div>
				</div>
				<h4 class="ui dividing header">Venue type <button type="button" class="ui mini green basic button select-all">Select all</button>
  												<button type="button" class="ui mini red basic button deselect-all">Deselect all</button></h4>
				<div class="pill-tab">
					<div style="display: inline-block;">
						<?php
						$pubs = pods('publication', array("limit"=> -1, "select" => 'pub_venue_type.meta_value AS pub_venue_type'));
						$counter=1;
										//print_r($pubs);
						while ( $pubs->fetch() ) { if($pubs->display( 'pub_venue_type' ) != ''){?>
							<label class="PillList-item">
								<input type="checkbox" name="pub_venue_type[]" value="<?php echo $pubs->field( 'pub_venue_type' );?>">
								<span class="PillList-label"><?php echo $pubs->display( 'pub_venue_type' );?>
								<span class="Icon Icon--checkLight Icon--smallest"><i class="fa fa-check"></i></span>
							</span>
						</label>
					<?php }}?>
					</div>
				</div>
			<h4 class="ui dividing header">Tag <button type="button" class="ui mini green basic button select-all">Select all</button>
  												<button type="button" class="ui mini red basic button deselect-all">Deselect all</button></h4>
			<div class="pill-tab">
				<div style="display: inline-block;">
					<?php
					$pubs = pods('publication', array("limit"=> -1, 'select' => 'pub_publication_tags.slug AS pub_tag_slug, pub_publication_tags.name AS pub_tag'));
										//print_r($pubs);
					while ( $pubs->fetch() ) { if($pubs->display( 'pub_tag' ) != ''){ ?>
						<label class="PillList-item">
							<input type="checkbox" name="pub_tag[]" value="<?php echo $pubs->display( 'pub_tag_slug' );?>">
							<span class="PillList-label"><?php echo $pubs->display( 'pub_tag' );?>
							<span class="Icon Icon--checkLight Icon--smallest"><i class="fa fa-check"></i></span>
						</span>
					</label>
				<?php }}?>
			</div>
		</div>
		<?php
		//$nonce = wp_create_nonce("filter_publications_nonce");
		?>
		<input type="hidden" name="action" value="filter_publications">
		<!-- <input type="hidden" name="filter_publications_nonce" value="<?php echo $nonce;?>"> -->

		<?php if(isset($_GET['tag'])){ ?>

		<input type="hidden" name="default_tag" value="<?php echo $_GET['tag']; ?>">

		<?php }?>
		<button class="ui blue button" style="margin-bottom: 20px; padding: 10px 20px; box-shadow: none;" type="submit">Filter
		</button>
	</form>



	<div id="publications-results">
		<ul>
			<!-- <?php

			$pubs = pods('publication', array());

			while($pubs->fetch()){ ?>

				<li><?php echo $pubs->display('pub_display_format'); ?></li>

			<?php }?> -->

		</ul>
	</div>



<!-- 					<?php query_posts('cat=4'); ?>
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); $project_pi = get_field( "project_pi" ); $project_students = get_field( "project_students" ); ?>

						<a class="ui fluid card" href="<?php the_permalink();?>">
							<div class="image" style="background: black;">
								<img src="<?php the_post_thumbnail_url();?>" style="margin: 0 auto; width: unset;">
							</div>
							<div class="content">
								<h4 class="header"><?php the_title(); ?></h4>
								<div class="meta">
							      <span class="date">
							      	<strong>Principal Investigator(s): </strong><?php echo $project_pi;?>
							      	<strong>	&nbsp;	&nbsp;	&nbsp;	&nbsp;Student(s): </strong><?php echo $project_students;?>
							  	  </span>
							    </div>
								<div class="description">
									<?php the_excerpt(); ?>
								</div>
							</div>
							<div class="extra content">
									<i class="fas fa-file-alt icon"></i>
									3 publications
							</div>
						</a>
						<?php endwhile; endif; ?> -->


					</div>

				</div>

			</div>

			<?php get_footer(); ?>