</div>
</div>

<footer id="main-footer">

    <div class="bottom-footer row">
        <div class="column small-order-1 medium-order-2 small-12 medium-6 large-4 ">
            <div>
                <span class="title">Contact</span>
                <p>
                    Postal (Visiting) Address: <br>UCLA, Electrical Engineering,<br> 56-125B (54-130B) Engineering IV,<br> Los Angeles, CA 90095-1594
                </p>
                <a href="mailto:danijela@ee.ucla.edu"><i class="fas fa-envelope icon"></i>danijela@ee.ucla.edu</a><br>
                <a href="www.ipsl.lk/index.php/events/physics-olympiad"><i class="fas fa-globe icon"></i>+1 310 206 8856</a>
            </div>
        </div>

        <div class="column small-order-1 medium-order-2 small-12 medium-6 large-4 ">
            <div>
                <span class="title">Follow us</span>
                <div>
                    <!-- <a href="https://www.facebook.com" class="ui circular facebook icon-button">
                        <i class="fab fa-facebook icon"></i>
                    </a> -->
                    <a href="https://www.twitter.com" class="ui circular twitter icon-button">
                        <i class="fab fa-twitter icon"></i>
                    </a>
                </div>
            </div>

        </div>

        <div class="column small-order-1 medium-order-2 small-12 medium-6 large-4 ">
            <div>
                <span class="title">Quick links</span>
                <ul>
                    <li>
                        <a href="/research">
                            Research
                        </a>
                    </li>

                    <li>
                        <a href="/people">
                            People
                        </a>
                    </li>

                    <li>
                        <a href="/publications">
                            Publications
                        </a>
                    </li>

                    <li>
                        <a href="/news">
                            News
                        </a>
                    </li>

                </ul>
            </div>

        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</main>

<script src="<?php echo get_template_directory_uri();?>/eventscope_landing_files/web.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/eventscope_landing_files/dependencies/classie.js"></script>
<script src="<?php echo get_template_directory_uri();?>/eventscope_landing_files/bee3D.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script>
    if( !window.jQuery ) document.write('<script src="js/jquery-3.0.0.min.js"><\/script>');
</script>
<script src="<?php echo get_template_directory_uri();?>/eventscope_landing_files/modernizr-custom.js"></script>
<script src="<?php echo get_template_directory_uri();?>/eventscope_landing_files/main.js"></script>
<script src="<?php echo get_template_directory_uri();?>/eventscope_landing_files/tablesort.js"></script>
<script src="<?php echo get_template_directory_uri();?>/eventscope_landing_files/components/accordion.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/eventscope_landing_files/custom.js"></script>
<script>

    //$('table').tablesort();
    var demo = document.getElementById('demo');

    var slider = new Bee3D(demo, {
        effect: 'classic',
        autoplay: {
            enabled: true,
            speed: 5000,
            pauseHover: true
        },
        loop: {
            enabled: true,
            continuous: false
        }
        // ... more options here
    });


</script>

<script>
$(document).ready(function(){
  $('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
});
</script>


<!--<script src="<?php echo get_template_directory_uri();?>//code.tidio.co/wbmjvesfhdnodljozagxxoxa2nis3p4m.js"></script>-->
</body></html>