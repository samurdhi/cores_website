<?php get_header(); ?>

<div class="row" style="margin-top: 20px;">
    <div class="small-12 medium-10 large-12 align-center columns">
        <div class="ui segment">
          <p>Cognitive Reconfigurable Embedded Systems Lab is a unique research entity within UCLA, which focuses on research in Cognitive Radio and Cognitive Radio-like systems. We are interested in all modern radio technologies, with the emphasis on systems that enable more efficient utilization of spectrum. Our group headed by Prof. Danijela Čabrić is a vibrant and young group of graduate students, international visitors and researchers.</p>
        </div>
    </div>
</div>

<div class="row" style="margin-top: 20px;">
    <div class="small-12 medium-10 large-6 align-center columns">
      <div class="ui segment" style="height: 100%;">
        <h3>News</h3>
          <div class="ui feed">
<!--               <div class="current event">
                <div class="content">
                  <div class="date">
                    May, 2020
                  </div>
                  <div class="summary">
                    Congratulations to <a>Han Yan</a> for for winning the Best PhD Dissertation Research Award in Signals & Systems!
                  </div>
                </div>
              </div> -->


              <?php

                //$posts = pods('research', array("limit"=> -1));

                $news = pods('news_item', array("limit"=> -1, "orderby" => 'news_item_date.meta_value DESC',));
                $iter=0;

                while ($news->fetch()) {

                  $phpdate = strtotime( $news->field('news_item_date') );
                  $mysqldate = date( 'F, Y', $phpdate );

                  if($news->field('news_item_archived')) continue;

                  ?>

                  <div class="<?php if($iter==0) echo "current";?> event">
                    <div class="content">
                      <div class="date">
                        <?php echo $mysqldate; ?>
                      </div>
                      <div class="summary">
                        <?php echo $news->display('news_item_display_text'); ?>
                      </div>
                    </div>
                  </div>
                <?php
                  $iter++;
                }?>

<!--               <div class="event">
                <div class="content">
                  <div class="date">
                    January, 2020
                  </div>
                  <div class="summary">
                    Congratulations to <a>Enes Krijestorac</a> for securing the 1st place in the Ph.D. Preliminary Examination (Signals & Systems track)!
                  </div>
                </div>
              </div>
              <div class="event">
                <div class="content">
                  <div class="date">
                    October, 2019
                  </div>
                  <div class="summary">
                    Welcome to our new Fulbright visiting student <a>Agon Memedi</a> from <a>Paderborn University</a>
                  </div>
                </div>
              </div>
              <div class="event">
                <div class="content">
                  <div class="date">
                    September, 2019
                  </div>
                  <div class="summary">
                    Welcome to our new graduate students: <a>Benjamin Domae</a> and <a>Samurdhi Karunaratne</a>!
                  </div>
                </div>
              </div>
              <div class="event">
                <div class="content">
                  <div class="date">
                    September, 2019
                  </div>
                  <div class="summary">
                    CORES lab receives a new <a>NSF award</a> for Circuits and Systems Design for UAV Swarm Enabled Communications.
                  </div>
                </div>
              </div>
              <div class="event">
                <div class="content">
                  <div class="date">
                    May, 2019
                  </div>
                  <div class="summary">
                    Congratulations to <a>Han Yan</a> on winning the prestigious <a>Qualcomm Innovation Fellowship</a>!
                  </div>
                </div>
              </div>
              <div class="event">
                <div class="content">
                  <div class="date">
                    February, 2019
                  </div>
                  <div class="summary">
                    Congratulations to <a>Ghaith Hattab</a> on defending his Ph.D. thesis!
                  </div>
                </div>
              </div>
              <div class="event">
                <div class="content">
                  <div class="date">
                    February, 2019
                  </div>
                  <div class="summary">
                    Congratulations to <a>Samer Hanna</a> and Prof. <a>Danijela Cabric</a> for winning the Best Paper Award at IEEE ICNC 2019 for the paper "Deep Learning Based Transmitter Identification using Power Amplifier Nonlinearity"!
                  </div>
                </div>
              </div> -->


          </div>
        
      </div>
    </div>

        <div class="small-12 medium-10 large-6 align-center columns">


                <div class="ui fluid  card">
                  <div class="image">
                    <img src="<?php echo get_template_directory_uri();?>/assets/Group_Photo_April_2017.jpg">
                  </div>
                  <div class="content">
                    <a class="header">UCLA Cores Lab (as of April 2018)</a>
                    <div class="description">
                      from left to right Benjamin Domae, Veljko Boljanovic, Yan Han, Ghaith Hattab, Danijela Čabrić, Han Yan, Shailesh Chaudhari, and Samer Hanna
                    </div>
                  </div>
                </div>



        </div>

</div>


<div class="row" style="margin-top: 20px;">
      <div class="small-12 medium-10 large-12 align-center columns">
        <div class="ui segment">
          <h3>Our Research</h3>
          <p>We perform theoretical research with strong emphasis on the implementation and practicality of the solutions for future Cognitive Radio systems. We also conduct practical experiments using a variety of Cogntive Radio Testbeds in conjunction with a rapid prototyping design environement. We deal with anything related to Opportunistic Spectrum Access, Dynamic Spectrum Access, Reconfigurable Radios, Policy-based Radios, and Underlay and Overlay Radio Systems. All information about our current projects, with brief descriptions of the results and links to current publications, can be found on the Research page.</p>
          <a class="ui blue button" href="/research" style="margin-top: 10px;" ><i class="fas fa-location-arrow icon"></i> See current projects  </a>
        </div>
    </div>
</div>

<div class="row" style="margin-top: 20px;">

    <div class="small-12 medium-10 large-6 align-center columns">
        <div class="column">
            <div class="title small-12 medium-12 large-11 columns">
                <h2>News articles</h2>
            </div>
            <div class="small-12 medium-12 large-11 columns">

                <div id="carousel_wrap">


                    <div id="demo" class="bee3D--parent">

                        <?php query_posts('cat=2'); ?>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                          <section class="bee3D--slide">
                            <a class="ui raised fluid card" href="<?php the_permalink();?>">
                                <div class="image" style="background: black;">
                                    <img src="<?php the_post_thumbnail_url();?>" style="margin: 0 auto; width: unset;  border-radius: 0;">
                                </div>
                                <div class="content">
                                    <div class="header"><?php echo wp_trim_words( get_the_title(), 12); ?></div>
                                </div>
                            </a>
                          </section>
                        <?php endwhile; endif; ?>
                    </div>
                    <!-- <span class="bee3D--nav bee3D--nav__prev"></span>
                    <span class="bee3D--nav bee3D--nav__next"></span> -->
                </div>

            </div>
        </div>
    </div>

    <div class="small-12 medium-10 large-6 columns">
        <div class="ui positive message">
          <div class="header">
            Postdoc position available
          </div>
          <p>Postdoctoral scholar position (starting January 2020) available in Prof. Cabric’s research group at University of California, Los Angeles (UCLA), in the areas of millimeter wave communications, machine learning for spectrum sharing and distributed communications and sensing (funded by DARPA and NSF). Candidates with strong background in wireless communications, signal processing and machine learning are encouraged to apply. Prior experience in these research areas is preferred. Interested applicants should send their CV, along with the names of at least one reference, to Prof. Cabric via email (danijela@ee.ucla.edu).</p>
        </div>
    </div>





        <div class="small-12 medium-12 large-12 align-center columns" style="margin-top: 40px;">
        <div class="column">
            <div class="small-12 medium-12 large-11 columns">

                    <h2>We are thankful to our sponsors</h2>

                <div class="ui four cards" style="margin-top: 20px;">
                      <div class="card">
                        <div class="image">
                          <img src="<?php echo get_template_directory_uri();?>/assets/Caltrans_logo.png">
                        </div>
                      </div>
                      <div class="card">
                        <div class="image">
                          <img src="<?php echo get_template_directory_uri();?>/assets/DAPRA_logo.jpg">
                        </div>
                      </div>
                      <div class="card">
                        <div class="image">
                          <img src="<?php echo get_template_directory_uri();?>/assets/NSF.gif">
                        </div>
                      </div>
                      <div class="card">
                        <div class="image">
                          <img src="<?php echo get_template_directory_uri();?>/assets/JUMP.png">
                        </div>
                      </div>
                      <div class="card">
                        <div class="image">
                          <img src="<?php echo get_template_directory_uri();?>/assets/Cisco_logo.gif">
                        </div>
                      </div>
                      <div class="card">
                        <div class="image">
                          <img src="<?php echo get_template_directory_uri();?>/assets/Iarpa.jpg">
                        </div>
                      </div>
                    </div>
            </div>
    </div>

        </div>

</div>

<?php get_footer(); ?>