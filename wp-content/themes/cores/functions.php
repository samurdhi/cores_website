<?php

include 'custom-shortcodes.php';


//error_log("sdfdsfds");


add_action('pods_api_post_save_pod_item_issue', 'my_post_save_function', 10, 3);

function my_post_save_function($pieces, $is_new_item, $id ) {


    $to = 'samurdhilbk@gmail.com';
    
    $message = $pieces['fields']['issue_description']['value'];


    if ( ! $is_new_item ) {
        $subject = 'Updated '.$pieces['fields']['issue_type']['value'].': '. $pieces['fields']['issue_title']['value'];
    }
    else{
        $subject = 'New '.$pieces['fields']['issue_type']['value'].': '. $pieces['fields']['issue_title']['value'];
    }

    error_log($to.' '.$subject.' '.$message);

    wp_mail($to, $subject, $message );

}

add_filter( 'pods_api_pre_save_pod_item_publication', 'slug_set_title', 10, 2);
function slug_set_title($pieces, $is_new_item) {

    //trigger_error("Fatal error", E_USER_ERROR);

    //error_log(var_dump($pieces));

    //trigger_error("Fatal error", E_USER_ERROR);

    //check if is new item, if not return $pieces without making any changes
    if ( ! $is_new_item ) {
        return $pieces;
    }
    //make sure that all three fields are active
    $fields = array( 'post_title', 'pub_name' );
    foreach( $fields as $field ) {
        if ( ! isset( $pieces[ 'fields_active' ][ $field ] ) ) {
            array_push ($pieces[ 'fields_active' ], $field );
        }
    }

    //trigger_error("Fatal error", E_USER_ERROR);

    //set variables for fields empty first for saftey's sake
    $sandwich = '';
    //get value of "sandwich" if possible
    if ( isset( $pieces[ 'fields' ][ 'pub_name' ] ) && isset( $pieces[ 'fields'][ 'pub_name' ][ 'value' ] ) && is_string( $pieces[ 'fields' ][ 'pub_name' ][ 'value' ] ) ) {
        $sandwich = $pieces[ 'fields' ][ 'pub_name' ][ 'value' ];
    }
    //set post title using $sandwich and $beverage
    $pieces[ 'object_fields' ][ 'post_title' ][ 'value' ] = $sandwich;
    //return $pieces to save
    return $pieces;
}

add_filter( 'pods_api_pre_save_pod_item_issue', 'issue_set_title', 10, 2);
function issue_set_title($pieces, $is_new_item) {

    if ( ! $is_new_item ) {

        return $pieces;
    }
    //make sure that all three fields are active
    $fields = array( 'post_title', 'issue_title' );
    foreach( $fields as $field ) {
        if ( ! isset( $pieces[ 'fields_active' ][ $field ] ) ) {
            array_push ($pieces[ 'fields_active' ], $field );
        }
    }
    $sandwich = '';
    //get value of "sandwich" if possible
    if ( isset( $pieces[ 'fields' ][ 'issue_title' ] ) && isset( $pieces[ 'fields'][ 'issue_title' ][ 'value' ] ) && is_string( $pieces[ 'fields' ][ 'issue_title' ][ 'value' ] ) ) {
        $sandwich = $pieces[ 'fields' ][ 'issue_title' ][ 'value' ];
    }
    //set post title using $sandwich and $beverage
    $pieces[ 'object_fields' ][ 'post_title' ][ 'value' ] = $sandwich;
    //return $pieces to save
    return $pieces;
}


function pods_ui_test ( $ui, $pod_pod, $pod ) {

    trigger_error("Fatal error", E_USER_ERROR);

    // Test on UI Column
    $ui[ 'fields' ][ 'add' ] = array(
        'pub_year' => 'City',
    );
    $ui[ 'fields' ][ 'manage' ] = array(
        'pub_year' => 'City',
    );

    return $ui;
}

add_filter( 'pods_admin_ui_publication', 'pods_ui_test', 10, 3 );


function initialize_admin () {

    //set Your icon URL here
    $icon = '';

    //adding page object -> http://codex.wordpress.org/Function_Reference/add_object_page
    add_object_page('Publications', 'Publications', 'edit_posts', 'publication', 'publications_menu', $icon);

    add_object_page('Issues', 'Issues', 'edit_posts', 'issue', 'issues_menu', $icon);

    //in order to not duplicate top menu - first child menu have the same slug as parent
    //http://codex.wordpress.org/Function_Reference/add_submenu_page

    //add_submenu_page('other-items', 'Actions', 'Actions', 'manage_options', 'slk-actions', 'your_function');

}


function publications_menu(){       
    $object = pods('publication');

    $fields = array();

    // iterate through the fields in this pod 
    foreach($object->fields as $field => $data) {
        $fields[$field] = array('label' => $data['label']);
    }

    // exclude a specific field by field name 
    
    // customize the label for a particular field 


    // hide some fields on edit screen but still have them on the add screen 
    $edit_fields = $fields;
    //unset($edit_fields['field_name']);

    // fields visible on manage screens 
    $manage_fields = $fields;


    $manage_fields['post_date'] = array( 'label' => 'Added on');
    $manage_fields['post_author'] = array( 'label' => 'Added by');


    unset($manage_fields['pub_year']);
    unset($manage_fields['pub_display_format']);
    unset($manage_fields['pub_url']);
    unset($manage_fields['pub_submission_only']);
    unset($manage_fields['pub_publication_tags']);

    $object->ui = array(
        'fields' => array(
            'add' => $fields,
            'edit' => $edit_fields,
            'manage' => $manage_fields,
        ),
        'filters' => array('pub_venue_type', 'pub_project'),
        //other parameters
    );

    pods_ui($object);
}

function issues_menu(){       
    $object = pods('issue');

    $fields = array();

    // iterate through the fields in this pod 
    foreach($object->fields as $field => $data) {
        $fields[$field] = array('label' => $data['label']);
    }

    // exclude a specific field by field name 
    
    // customize the label for a particular field 
    //$fields['field_name'] = array( 'label' => 'some_different_label');

    // hide some fields on edit screen but still have them on the add screen 
    $edit_fields = $fields;
    //unset($edit_fields['field_name']);

    // fields visible on manage screens 
    $manage_fields = $fields;

    $manage_fields['post_date'] = array( 'label' => 'Added on');
    $manage_fields['post_author'] = array( 'label' => 'Added by');

    unset($manage_fields['issue_description']); 

    $object->ui = array(
        'fields' => array(
            'add' => $fields,
            'edit' => $edit_fields,
            'manage' => $manage_fields,
        ),
        'filters' => array('issue_type', 'issue_status'),
        //other parameters
    );

    pods_ui($object);
}


add_action('admin_menu', 'initialize_admin');


add_theme_support( 'post-thumbnails' );

function my_acf_load_field( $field ) {
    $field['required'] = false;



    $field['choices'] = array(
    	'default'   => '',
    );

    $args = array( 'category' => get_cat_ID('People'), 'numberposts' => -1);

    $myposts = get_posts( $args );

    foreach ( $myposts as $post ) {
        $field['choices'][get_permalink($post)]=$post->post_title;
    }

    return $field;
}




// Apply to select fields.
add_filter('acf/load_field/name=user_profile_page', 'my_acf_load_field');

//error_log("Test", 3, "./my-errors.log");

add_action("wp_ajax_filter_publications", "filter_publications");
add_action("wp_ajax_nopriv_filter_publications", "filter_publications");

function filter_publications() {

    //error_log(var_dump($_POST), 3, "../my-errors.log");

   // if ( !wp_verify_nonce( $_REQUEST['nonce'], "filter_publications_nonce")) {
   //    exit("No naughty business please");
   //    die();
   // }   

    //error_log($_POST['pub_year']);

    $where_array = array();

    if(isset($_POST['pub_year'])){
        error_log("pub_year not empty");
        $where_array[]=array(
            "field"   => "pub_year.meta_value",
            "value"   => $_POST['pub_year'],
            "compare" => "="
        );
    }

    if(isset($_POST['pub_venue_type'])){
        error_log("Venues not empty");
        $where_array[]=array(
            "field"   => "pub_venue_type.meta_value",
            "value"   => $_POST['pub_venue_type'],
            "compare" => "="
        );
    }

    if(isset($_POST['pub_tag'])){
        error_log("pub_publication_tags not empty");
        $where_array[]=array(
            "field"   => "pub_publication_tags.slug",
            "value"   => $_POST['pub_tag'],
            "compare" => "="
        );
    }


    $pubs = pods('publication', array("limit"=> -1, "orderby" => 'pub_publication_date.meta_value DESC', "where" => $where_array));

    $results_array = array();


    $years = array();
    $venues = array();



    while($pubs->fetch()){
        $curr_pub = [];
        //error_log($pubs->display('pub_display_format'), 3, "../my-errors.log");
        $curr_pub['name'] = $pubs->display('pub_name');
        $years[] = $pubs->display('pub_year');
        $venues[] = $pubs->display('pub_venue_type');
        $curr_pub['year'] = $pubs->display('pub_year');
        $curr_pub['venue_type'] = $pubs->display('pub_venue_type');
        $curr_pub['tags'] = $pubs->field('pub_publication_tags');
        $curr_pub['url'] = $pubs->field('pub_url');
        $curr_pub['display_format'] = $pubs->display('pub_display_format');
        $curr_pub['innerHTML'] = processPublication($curr_pub['display_format'], $pubs->display('pub_name'), $pubs->field('pub_url'), $pubs->field('pub_publication_tags'));
        $results_array[] = $curr_pub;
    }


    $result['years'] = array_unique($years);
    $result['venues'] = array_unique($venues);
    $result['return'] = $results_array;
    // $result['log'][] = $_POST['pub_year'];
    // $result['log'][] = $_POST['pub_venue_type'];
    // $result['log'][] = $_POST['pub_tag'];
    // $result['log'][] = $pubs->total();

    echo json_encode($result);

    die();

   // $vote_count = get_post_meta($_REQUEST["post_id"], "votes", true);
   // $vote_count = ($vote_count == ’) ? 0 : $vote_count;
   // $new_vote_count = $vote_count + 1;

   // $vote = update_post_meta($_REQUEST["post_id"], "votes", $new_vote_count);

   // if($vote === false) {
   //    $result['type'] = "error";
   //    $result['vote_count'] = $vote_count;
   // }
   // else {
   //    $result['type'] = "success";
   //    $result['vote_count'] = $new_vote_count;
   // }

   // if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
   //    $result = json_encode($result);
   //    echo $result;
   // }
   // else {
   //    header("Location: ".$_SERVER["HTTP_REFERER"]);
   // }

   // die();

}


add_action("wp_ajax_filter_projects", "filter_projects");
add_action("wp_ajax_nopriv_filter_projects", "filter_projects");

function filter_projects() {

    //error_log(var_dump($_POST), 3, "../my-errors.log");

   // if ( !wp_verify_nonce( $_REQUEST['nonce'], "filter_publications_nonce")) {
   //    exit("No naughty business please");
   //    die();
   // }   



    $results_array = array();

    if(isset($_POST['research_areas'])){
        $mypod1 = pods('research_area', array("limit"=> -1, "where" => array(
            array(
                "field"   => "research_area_slug.meta_value",
                "value"   => $_POST['research_areas'],  
                "compare" => "="
            )
        )));


        $ra_ids = array();

        while($mypod1->fetch()){
            $ra_ids[] = $mypod1->id();
        }

        // /var_dump($ra_ids);

        $ridd = [];

        for ($ii=0; $ii < count($ra_ids); $ii++) {
            $ra_id = $ra_ids[$ii];
            $podd = pods('research', array("limit"=> -1, "where" => array(
                'relation' => 'AND',
                array(
                    "field"   => "project_research_area.id",
                    "value"   => $ra_id,  
                    "compare" => "="
                ),
                array(
                    'relation' => 'OR',
                    array(
                        "field"   => "project_archived.meta_value",
                        "value"   => 1,
                        "compare" => "!="
                    ),
                    array(
                        "field"   => "project_archived.meta_value",
                        "compare"   => 'NOT EXISTS',
                    ),
                )
            )));

            $rid=array();

            while($podd->fetch()){
                $rid[]=$podd->id();
            }

            if($ii==0) $ridd = $rid;
            else $ridd = array_intersect($ridd, $rid);

        }

        if(count($ridd)){
            $mypod = pods('research', array("limit"=> -1, "where" => array(
                'relation' => 'AND',
                array(
                    "field"   => "t.ID",
                    "value"   => $ridd,  
                    "compare" => "="
                ),
                array(
                    'relation' => 'OR',
                    array(
                        "field"   => "project_archived.meta_value",
                        "value"   => 1,
                        "compare" => "!="
                    ),
                    array(
                        "field"   => "project_archived.meta_value",
                        "compare"   => 'NOT EXISTS',
                    ),
                )
            )));


            while($mypod->fetch()){
                $curr_pub = [];

                $pic = $mypod->field('project_thumbnail');
                $pubs = pods('publication', array("where" => array(
                                    array(
                                        "field"   => "pub_project.id",
                                        "value"   => $mypod->id(),
                                        "compare" => "="
                                    ),
                                )));

                $curr_pub['url'] = get_permalink($mypod->id());
                $curr_pub['name'] = $mypod->display('project_name');
                $curr_pub['thumbnail'] = wp_get_attachment_url($pic['ID']);
                $curr_pub['num_pub'] = $pubs->total();
                $results_array[] = $curr_pub;
            }
        }


    }
    else{

        $mypod = pods('research', array("limit"=> -1, "where" => array(
            'relation' => 'OR',
            array(
                "field"   => "project_archived.meta_value",
                "value"   => 1,
                "compare" => "!="
            ),
            array(
                "field"   => "project_archived.meta_value",
                "compare"   => 'NOT EXISTS',
            ),
        )));


        while($mypod->fetch()){
            $curr_pub = [];

            $pic = $mypod->field('project_thumbnail');
            $pubs = pods('publication', array("where" => array(
                                array(
                                    "field"   => "pub_project.id",
                                    "value"   => $mypod->id(),
                                    "compare" => "="
                                ),
                            )));

            $curr_pub['url'] = get_permalink($mypod->id());
            $curr_pub['name'] = $mypod->display('project_name');
            $curr_pub['thumbnail'] = wp_get_attachment_url($pic['ID']);
            $curr_pub['num_pub'] = $pubs->total();
            $results_array[] = $curr_pub;
        }
    }

    

    // $result['return'] = $results_array;

    $result['ra_ids'] = $ridd;
    $result['return'] = $results_array;

    echo json_encode($result);
 
    die();

}


function SearchString($str, $pat){
    $retVal = array();
    $M = strlen($pat);
    $N = strlen($str);
    $i = 0;
    $j = 0;
    $lps = array();

    ComputeLPSArray($pat, $M, $lps);

    while ($i < $N)
    {
        if ($pat[$j] == $str[$i])
        {
            $j++;
            $i++;
        }

        if ($j == $M)
        {
            array_push($retVal, $i - $j);
            $j = $lps[$j - 1];
        }

        else if ($i < $N && $pat[$j] != $str[$i])
        {
            if ($j != 0)
                $j = $lps[$j - 1];
            else
                $i = $i + 1;
        }
    }

    return $retVal;
}

function ComputeLPSArray($pat, $m, &$lps){
    $len = 0;
    $i = 1;

    $lps[0] = 0;

    while ($i < $m)
    {
        if ($pat[$i] == $pat[$len])
        {
            $len++;
            $lps[$i] = $len;
            $i++;
        }
        else
        {
            if ($len != 0)
            {
                $len = $lps[$len - 1];
            }
            else
            {
                $lps[$i] = 0;
                $i++;
            }
        }
    }
}

function seoUrl($string) {
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}

function processPublication($pub_str, $pub_title, $pub_url, $pub_tags){
    $value = SearchString($pub_str, $pub_title);

    $colors = array('teal', 'blue', 'green', 'violet');

    $html_ul = '';

    $html_ul .= '<div class="ui fluid card" style="margin-bottom: 0.5em;"><div class="content"><div class="description">'.
                                                    processAuthorString(substr($pub_str, 0, $value[0])).'<a class="boldref" target="_blank" href="'.$pub_url.'">'.$pub_title.'</a>'.substr($pub_str, $value[0]+strlen($pub_title))
                                                    .'</div></div>';
                                        
    $html_tags = '';
    shuffle($colors);
    $h = 0;
    foreach($pub_tags as $tag){
        //echo $tag;

        // $tag_obj = pods('publication_tag', array("where" => array(
        //     array(
        //         "field"   => "pub_tag_name.meta_value",
        //         "value"   => $tag['name'],
        //         "compare" => "="
        //     )
        // )));

        // print_r($tag_obj);

        $html_tags .= '<a href="/publicatons/?tag='.seoUrl($tag['name']).'" class="ui ' . $colors[$h%count($colors)] . ' label">' . $tag['name'] . '</a>';
        $h += 1;
    }

    if(!empty($html_tags)){
        $html_ul .= '<div class="extra content">';
        $html_ul .=  $html_tags;
        $html_ul .= '</div>';
    }

    $html_ul .= '</div>';

    return $html_ul;
}

// function processPublicationIfAuthor($pub_str, $pub_title, $pub_url, $pub_tags, $author_name){

//     return processPublication($pub_str, $pub_title, $pub_url, $pub_tags);
// }

function processPublicationIfAuthor($pub_str, $pub_title, $pub_url, $pub_tags, $author_name){

    $name_pieces = explode(' ', $author_name);

    $first_name = $name_pieces[0];
    $last_name = end($name_pieces);
    reset($name_pieces);

    if(preg_match('/'.$last_name.'/', $pub_str)){
        return processPublication($pub_str, $pub_title, $pub_url, $pub_tags);
    }

    return '';
}

// function processPublicationIfAuthor($pub_str, $pub_title, $pub_url, $pub_tags, $author_name){

//     $value = SearchString($pub_str, $pub_title);

//     if(isAuthorHere(substr($pub_str, 0, $value[0]), $author_name)){
//         return processPublication($pub_str, $pub_title, $pub_url, $pub_tags);
//     }
//     return '';
// }

function isAuthorHere($auth_string, $author_name){
    $authors = preg_split("/,|and/", $auth_string);

    //print_r($authors);

    for ($i=0; $i < count($authors); $i++) { 
        $author = $authors[$i];
        
        $str = trim($author);
        if(preg_match('/[A-Za-z\.\s]+/', $str)){
          //echo trim($author).'<br>';

            $name_pieces = explode(' ', $str);

            //print_r($name_pieces);

            $first_char_of_first_name = $name_pieces[0][0];
            $last_name = end($name_pieces);
            reset($name_pieces);

            if(preg_match('/'.$first_char_of_first_name.'\w+\s*'.$last_name.'/', $author_name)){
                return true;
            }

        }
    }

    return false;
}

function title_filter( $where, &$wp_query )
{
    global $wpdb;

    if ( $search_term = $wp_query->get( 'search_prod_title' ) ) {

        //echo $search_term.'<br>';

        $name_pieces = explode(' ', $search_term);

        //print_r($name_pieces);

        $first_char_of_first_name = $name_pieces[0][0];
        $last_name = end($name_pieces);
        reset($name_pieces);

        //echo trim($first_char_of_first_name).'<br>';
        //echo $last_name.'<br>';

        $where .= ' AND ' . $wpdb->posts . '.post_title REGEXP \''.$first_char_of_first_name.'\\\w+\\\s*' . esc_sql( like_escape( $last_name ) ) . '\'';
    }
    return $where;
}



function getAuthURLIfExists($auth_name_string){

    // if(preg_match("/\w{1}\.\s{1}\w+/", $auth_name_string)){
    //     $name_pieces = explode(' ', $auth_name_string);
    //     $name_pieces[0][0];
    //     $name_pieces[-1];
    // }
    // else{
    //     $search_term = $auth_name_string;
    // }

    //echo $auth_name_string.'<br>';

    $args = array(
        'search_prod_title' => $auth_name_string,
        'post_status' => 'publish',
        'orderby'     => 'title', 
        'order'       => 'ASC',
        'cat' => 3,
    );

    add_filter( 'posts_where', 'title_filter', 10, 2 );
    $wp_query = new WP_Query($args);
    remove_filter( 'posts_where', 'title_filter', 10 );
    //print_r($wp_query);

    //echo $wp_query->post_count;

    while ( $wp_query->have_posts() ) {
        $wp_query->the_post();
        return get_the_permalink();
    }

    return '/people';
}

function processAuthorString($auth_string){
    $authors = preg_split("/,|and/", $auth_string);

    //print_r($authors);

    $orig = array();
    $rep = array();

    for ($i=0; $i < count($authors); $i++) { 
        $author = $authors[$i];
        
        $str = trim($author);
        if(preg_match('/[A-Za-z\.\s]+/', $str)){
          //echo trim($author).'<br>';
          $orig[] = trim($author);
          $rep[] = '<a href="'.getAuthURLIfExists(trim($author)).'">'.trim($author).'</a>';
        }
        //print_r($authors[$i]);
        // if(preg_match('/\w/', $author)){
        //     echo $author.'<br>';
        //     $orig[] = trim($author);
        //     $rep[] = '<a href="'.getAuthURLIfExists(trim($author)).'">'.trim($author).'</a>';
        // }
    }

    // foreach ($authors as $author) {

    //     if(preg_match('/\w/', $author)){
    //         //echo $author.'<br>';
    //         $orig[] = trim($author);
    //         $rep[] = '<a href="'.getAuthURLIfExists(trim($author)).'">'.trim($author).'</a>';
    //     }
    // }

    $new_auth_string = str_replace($orig, $rep, $auth_string);

    return $new_auth_string;
}

?>