<?php

function render_publications() {
	$pubs = pods('publication', array("where" => array(
            array(
                "field"   => "pub_project.id",
                "value"   => get_the_ID(),
                "compare" => "="
            ),
            // array(
            //     "field"   => "pub_year.meta_value",
            //     "value"   => 2019,
            //     "compare" => "="
            // )
        )));
	ob_start();
	//echo '<ul>';
	while ( $pubs->fetch() ) {?>
		<?php echo processPublication($pubs->display('pub_display_format'), $pubs->display('pub_name'), $pubs->display('pub_url'), $pubs->field('pub_publication_tags')); ?>
		<!-- <li><a target="_blank" href="<?php echo $pubs->display('pub_url')?>"><?php echo $pubs->display('pub_display_format')?></a></li> -->
	<?php }
	//echo '</ul>';
	return ob_get_clean();
}

add_shortcode('render_publications', 'render_publications');

function render_lab_location() {
	return '<div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=engineering%20iv%20&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://2torrentz.net">/torrentz2</a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style></div>';
}

add_shortcode('render_lab_location', 'render_lab_location');

function render_personal_publications() {
	$pubs = pods('publication', array("limit"=> -1, "orderby" => 'pub_publication_date.meta_value DESC'));

	$author_name = get_the_title();

	ob_start();
	//echo '<ul>';
	while ( $pubs->fetch() ) {?>
		<?php echo processPublicationIfAuthor($pubs->display('pub_display_format'), $pubs->display('pub_name'), $pubs->display('pub_url'), $pubs->field('pub_publication_tags'), $author_name); ?>
		<!-- <li><a target="_blank" href="<?php echo $pubs->display('pub_url')?>"><?php echo $pubs->display('pub_display_format')?></a></li> -->
	<?php }
	//echo '</ul>';
	return ob_get_clean();
}

add_shortcode('render_personal_publications', 'render_personal_publications');


function render_research_area_projects() {
	

	$projects = pods('research', array("where" => array(
		array(
			"field"   => "project_research_area.id",
			"value"   => get_the_ID(),
			"compare" => "="
		)
	)));


	$project_ids = array();

	while ( $projects->fetch() ) {
		$project_ids[] = $projects->id();
	}

	$pubs = pods('publication', array("where" => array(
		array(
			"field"   => "pub_project.id",
			"value"   => $project_ids,
			"compare" => "="
		),
	)));

	$posts = pods('research', array());

	ob_start();
	while ($posts->fetch()) {
		if(!in_array($posts->id(), $project_ids)) continue;
		$pic = $posts->field('project_thumbnail');
		$curr_pubs = pods('publication', array("where" => array(
			array(
				"field"   => "pub_project.id",
				"value"   => $posts->id(),
				"compare" => "="
			),
		)));

		echo '<div class="ui segment"						>
			<div class="ui items">
				<div class="item">';
					if(wp_get_attachment_url($pic['ID'])){
						echo '<div class="image">
							<img style="height: 100%;" src="'.wp_get_attachment_url($pic['ID']).'">
						</div>';
					}
					echo '<div class="content">
						<a target="_blank" href="'.get_permalink($posts->id()).'" class="header">'.$posts->display('project_name').'</a>
						<div class="meta">
							<span><i class="fas fa-file-alt icon"></i>
							'.$curr_pubs->total().'publication(s)</span>
						</div>
						<div class="description">
							<p></p>
						</div>
					</div>
				</div>
			</div>
		</div>';
	}
	return ob_get_clean();
}

add_shortcode('render_research_area_projects', 'render_research_area_projects');


function render_research_area_publications() {
	$projects = pods('research', array("where" => array(
		array(
			"field"   => "project_research_area.id",
			"value"   => get_the_ID(),
			"compare" => "="
		)
	)));


	$project_ids = array();

	while ( $projects->fetch() ) {
		$project_ids[] = $projects->id();
	}

	$pubs = pods('publication', array("where" => array(
		array(
			"field"   => "pub_project.id",
			"value"   => $project_ids,
			"compare" => "="
		),
	)));

	ob_start();

	while($pubs->fetch()){
		$curr_pub = [];
		$curr_pub['name'] = $pubs->display('pub_name');
		$years[] = $pubs->display('pub_year');
		$venues[] = $pubs->display('pub_venue_type');
		$curr_pub['year'] = $pubs->display('pub_year');
		$curr_pub['venue_type'] = $pubs->display('pub_venue_type');
		$curr_pub['tags'] = $pubs->field('pub_publication_tags');
		$curr_pub['url'] = $pubs->field('pub_url');
		$curr_pub['display_format'] = $pubs->display('pub_display_format');
		echo processPublication($curr_pub['display_format'], $pubs->display('pub_name'), $pubs->field('pub_url'), $pubs->field('pub_publication_tags'));
	}

	return ob_get_clean();
}

add_shortcode('render_research_area_publications', 'render_research_area_publications');

?>