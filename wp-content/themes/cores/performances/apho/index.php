<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>APhO performances</h3>
        </div>

        <div class="canvas-body">
            <div class="row">
                <div class="small-12 medium-10 large-6 align-center columns">

                    <h4>Medal Winners</h4>
                    <div class="table-wrapper">
                        <table class="ui celled unstackable table">
                            <thead>
                                <tr><th class="highlightDown">Year</th><th>Contestant</th><th>Rank</th><th>Award</th></tr>
                            </thead>
                            <tbody>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2014/.">2014</a></td>
                                    <td>Samurdhi Leuke Bandara Karunaratne</td>
                                    <td align="right">≥16</td><td><img src="<?php echo get_template_directory_uri();?>/assets/silver-medal.svg" width="9" height="9"> Silver Medal</td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2011/.">2007</a></td>
                                    <td>Buddhi Wejerathna</td>
                                    <td align="right">≥72</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2016/.">2014</a></td>
                                    <td>Mohammadu Rizan Mohamed Athif</td>
                                    <td align="right">≥73</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honorable Mention</td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2015/.">2014</a></td>
                                    <td>Nuwan Tharaka Perera</td>
                                    <td align="right">≥73</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honorable Mention</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2014/.">2014</a></td>
                                    <td>Ravindu Bangamuarachchi</td>
                                    <td align="right">≥73</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honorable Mention</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2017/.">2011</a></td>
                                    <td>Sivapalan Chelvaniththilan</td>
                                    <td align="right">≥81</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honorable Mention</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2017/.">2009</a></td>
                                    <td>Sameera Chandimal Ramasinghe</td>
                                    <td align="right">≥109</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honorable Mention</td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2016/.">2009</a></td>
                                    <td>Udari Tankana Samarasiri</td>
                                    <td align="right">≥109</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honorable Mention</td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2017/.">2008</a></td>
                                    <td>Kirthevasan Kandasamy</td>
                                    <td align="right">≥97</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honorable Mention</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="small-12 medium-10 large-6 align-center columns">
                    <h4>Delegations</h4>

                    <div class="vertical-buttons delegations" style="padding: 0 20px;">

                        <?php
                        $medal_counts = [
                            2018=>[0,0,0,0],2017=>[0,0,0,0],2016=>[0,0,0,0],2015=>[0,0,0,0],2014=>[0,1,0,3],2013=>[0,0,0,0],2012=>[0,0,0,0],2011=>[0,0,0,1],2010=>[0,0,0,0],2009=>[0,0,0,2],2008=>[0,0,0,1],2007=>[0,0,1,0],2006=>[0,0,0,0],2005=>[0,0,0,0]
                        ];
                        foreach ($medal_counts as $year=>$medals): ?>
                        <a class="plain button align-center align-middle" href="#">
                            <label><?php echo $year;?></label>
                            <div class="ui mini statistics" style="display: inline-block; margin: 0; margin-left: 5px;">
                                <div class="statistic">
                                    <div class="value">
                                        <img src="<?php echo get_template_directory_uri();?>/assets/gold-medal.svg" class="ui circular inline image">
                                    </div>
                                    <div class="label">
                                        <?php echo $medals[0];?>
                                    </div>
                                </div>
                                <div class="statistic">
                                    <div class="value">
                                        <img src="<?php echo get_template_directory_uri();?>/assets/silver-medal.svg" class="ui circular inline image">
                                    </div>
                                    <div class="label">
                                        <?php echo $medals[1];?>
                                    </div>
                                </div>
                                <div class="statistic">
                                    <div class="value">
                                        <img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" class="ui circular inline image">
                                    </div>
                                    <div class="label">
                                        <?php echo $medals[2];?>
                                    </div>
                                </div>
                                <div class="statistic">
                                    <div class="value">
                                        <img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" class="ui inline image">
                                    </div>
                                    <div class="label">
                                        <?php echo $medals[3];?>
                                    </div>
                                </div>

                            </div>
                        </a>
                        <?php endforeach ?>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>