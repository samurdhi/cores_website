<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/header.php'; ?>


<div class="row" style="margin-top: 50px;">

    <div class="ui fluid segment align-center canvas">

        <div class="canvas-title">
            <h3>IPhO performances</h3>
        </div>

        <div class="canvas-body">
            <div class="row">
                <div class="small-12 medium-10 large-6 align-center columns">

                    <h4>Medal Winners</h4>

                    <div class="table-wrapper">
                        <table class="ui celled unstackable table">
                            <thead>
                                <tr><th class="highlightDown">Year</th><th>Contestant</th><th class="sorted ascending">Rank</th><th>Award</th></tr>
                            </thead>
                            <tbody>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2014/.">2014</a></td>
                                    <td>Samurdhi Leuke Bandara Karunaratne</td>
                                    <td align="right">48</td><td><img src="<?php echo get_template_directory_uri();?>/assets/silver-medal.svg" width="9" height="9"> Silver Medal</td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2011/.">2011</a></td>
                                    <td>Sivapalan Chelvaniththilan</td>
                                    <td align="right">120</td><td><img src="<?php echo get_template_directory_uri();?>/assets/silver-medal.svg" width="9" height="9"> Silver Medal</td>
                                </tr>

                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2017/.">2017</a></td>
                                    <td>Dulanjana Minikirani Amaya Dharmasiri Kelegedara</td>
                                    <td align="right">≥137</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2017/.">2017</a></td>
                                    <td>Tharindu Madushanka Sandabandara Samarakoon Kimbulobbe Herath Mudiyanselage</td>
                                    <td align="right">≥137</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2016/.">2016</a></td>
                                    <td>Dasun Oshada Jayasinghe</td>
                                    <td align="right">155</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2016/.">2016</a></td>
                                    <td>Akila Manamendra Bandara Uyanwatta</td>
                                    <td align="right">195</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2015/.">2015</a></td>
                                    <td>Harshana Sumedha Weligampola</td>
                                    <td align="right">171</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2014/.">2014</a></td>
                                    <td>Mohammadu Rizan Mohamed Athif</td>
                                    <td align="right">144</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2014/.">2014</a></td>
                                    <td>Nuwan Tharaka Perera</td>
                                    <td align="right">153</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2014/.">2014</a></td>
                                    <td>Nisal Sandaras Ranasinghe</td>
                                    <td align="right">172</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2014/.">2014</a></td>
                                    <td>Ravindu Bangamuarachchi</td>
                                    <td align="right">184</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2013/.">2013</a></td>
                                    <td>Herath Mudiyanselage Kosala Sananthana Herath</td>
                                    <td align="right">185</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>


                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2011/.">2011</a></td>
                                    <td>Dananjaya Liyanage</td>
                                    <td align="right">145</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2011/.">2011</a></td>
                                    <td>W. Ralahamilage Chelaka Welikala</td>
                                    <td align="right">186</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2010/.">2010</a></td>
                                    <td>Yasura Gimhan Kossinhala Vithana</td>
                                    <td align="right">139</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2010/.">2010</a></td>
                                    <td>Tisura Dasith Gamage</td>
                                    <td align="right">186</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2008/.">2008</a></td>
                                    <td>Kirthevasan Kandasamy</td>
                                    <td align="right">150</td><td><img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" width="9" height="9"> Bronze Medal</td>
                                </tr>

                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2018/.">2018</a></td>
                                    <td>Mudith Nirmala Witharama</td>
                                    <td align="right">239</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honourable Mention</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2018/.">2018</a></td>
                                    <td>Ulpathagedara Kumuthu Mandara Premalal</td>
                                    <td align="right">248</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honourable Mention</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2018/.">2018</a></td>
                                    <td>Ginige Yasod Sandeepa</td>
                                    <td align="right">260</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honourable Mention</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2018/.">2018</a></td>
                                    <td>Dilini Sandunika Palihakkara</td>
                                    <td align="right">269</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honourable Mention</td>
                                </tr>

                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2017/.">2017</a></td>
                                    <td>Randula Abeyweera Kankanam Arachchige</td>
                                    <td align="right">≥239</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honourable Mention</td>
                                </tr>

                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2011/.">2011</a></td>
                                    <td>Jasenthu Kankanamlage Bandara</td>
                                    <td align="right">267</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honourable Mention</td>
                                </tr>

                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2010/.">2010</a></td>
                                    <td>Kanwel Gamage Don Nisal Menuka</td>
                                    <td align="right">201</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honourable Mention</td>
                                </tr>

                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2008/.">2008</a></td>
                                    <td>Nayana Rajapakse</td>
                                    <td align="right">188</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honourable Mention</td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2006/.">2006</a></td>
                                    <td>Tharidu Bandaragoda</td>
                                    <td align="right">208</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honourable Mention</td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2005/.">2005</a></td>
                                    <td>Nuwan Ganganath</td>
                                    <td align="right">168</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honourable Mention</td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2005/.">2005</a></td>
                                    <td>Gayan Jayasumana</td>
                                    <td align="right">204</td><td><img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" width="9" height="9"> Honourable Mention</td>
                                </tr>

                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2013/.">2013</a></td>
                                    <td>Thelasingha Hitihami Mudiyanselage Neelanga Chandrajith Thelasingha</td>
                                    <td align="right">280</td><td></td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2013/.">2013</a></td>
                                    <td>Ranasinghe Mudiyanselage Nipuna Indrachapa Herath Ranasinghe</td>
                                    <td align="right">311</td><td></td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2013/.">2013</a></td>
                                    <td>Jayan Chathuranga Vidanapathirana</td>
                                    <td align="right">334</td><td></td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2013/.">2013</a></td>
                                    <td>Edirisooriya Arachchi Appuhamilage Nelushi Eranga Edirisooriya</td>
                                    <td align="right">349</td><td></td>
                                </tr>
                                <tr class="doubleTopLine">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2012/.">2012</a></td>
                                    <td>Chanaka Manoj Singhabahu</td>
                                    <td align="right">≥272</td><td></td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2012/.">2012</a></td>
                                    <td>Dombagaha Gedara Prasad Randika Maithriepala</td>
                                    <td align="right">≥272</td><td></td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2012/.">2012</a></td>
                                    <td>Liraj Harsha Prabath Kodithuwakku</td>
                                    <td align="right">≥272</td><td></td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2012/.">2012</a></td>
                                    <td>M. Janidu Chandrashantha Gunarathna</td>
                                    <td align="right">≥272</td><td></td>
                                </tr>
                                <tr class="">
                                    <td align="center"><a href="<?php echo get_template_directory_uri();?>/performances/ipho/2012/.">2012</a></td>
                                    <td>Edurapotha Gamaralalage Inoka Amanthie Dharmasena</td>
                                    <td align="right">≥272</td><td></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="small-12 medium-10 large-6 align-center columns">
                    <h4>Delegations</h4>

                    <div class="vertical-buttons delegations" style="padding: 0 20px;">

                        <?php
                        $medal_counts = [
                            2018=>[0,0,0,4],2017=>[0,0,2,1],2016=>[0,0,2,0],2015=>[0,0,1,0],2014=>[0,1,4,0],2013=>[0,0,1,0],2012=>[0,0,0,0],2011=>[0,1,2,1],2010=>[0,0,2,1],2009=>[0,0,0,0],2008=>[0,0,1,1],2007=>[0,0,0,0],2006=>[0,0,0,1],2005=>[0,0,0,2]
                        ];
                        foreach ($medal_counts as $year=>$medals): ?>
                        <a class="plain button align-center align-middle" href="#">
                            <label><?php echo $year;?></label>
                            <div class="ui mini statistics" style="display: inline-block; margin: 0; margin-left: 5px;">
                                <div class="statistic">
                                    <div class="value">
                                        <img src="<?php echo get_template_directory_uri();?>/assets/gold-medal.svg" class="ui circular inline image">
                                    </div>
                                    <div class="label">
                                        <?php echo $medals[0];?>
                                    </div>
                                </div>
                                <div class="statistic">
                                    <div class="value">
                                        <img src="<?php echo get_template_directory_uri();?>/assets/silver-medal.svg" class="ui circular inline image">
                                    </div>
                                    <div class="label">
                                        <?php echo $medals[1];?>
                                    </div>
                                </div>
                                <div class="statistic">
                                    <div class="value">
                                        <img src="<?php echo get_template_directory_uri();?>/assets/bronze-medal.svg" class="ui circular inline image">
                                    </div>
                                    <div class="label">
                                        <?php echo $medals[2];?>
                                    </div>
                                </div>
                                <div class="statistic">
                                    <div class="value">
                                        <img src="<?php echo get_template_directory_uri();?>/assets/diploma.svg" class="ui inline image">
                                    </div>
                                    <div class="label">
                                        <?php echo $medals[3];?>
                                    </div>
                                </div>

                            </div>
                        </a>
                        <?php endforeach ?>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/parts/footer.php'; ?>